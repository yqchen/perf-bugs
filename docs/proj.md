# Commits by Projects

## NetworkManager

* [`c4a86eba52e4`](https://github.com/NetworkManager/NetworkManager/commit/c4a86eba52e4665159546293a5065bab12077b69)
* [`de9848aa66d2`](https://github.com/NetworkManager/NetworkManager/commit/de9848aa66d2ef97a53ad04c537df4859baea243)
* [`50403cccee28`](https://github.com/NetworkManager/NetworkManager/commit/50403cccee28c7dcd54b138a0d3b3f69ea0204fe)
* [`3751cceeec2b`](https://github.com/NetworkManager/NetworkManager/commit/3751cceeec2bdf246c04e5bd846086e6feeeb7b6)
* [`de5d07392da4`](https://github.com/NetworkManager/NetworkManager/commit/de5d07392da488113c956fbf1aca94fa280c3302)
* [`b913e1d641e9`](https://github.com/NetworkManager/NetworkManager/commit/b913e1d641e98c99f27936d76828998ae5534fbb) Use stack allocation
* [`18660604aae1`](https://github.com/NetworkManager/NetworkManager/commit/18660604aae1b21e3d628d8d566ca951892edcab) Do not rely on glib implementation
* [`4cdb84188154`](https://github.com/NetworkManager/NetworkManager/commit/4cdb8418815421931192aba90dc983dabcaffed2)
* [`e93abf0552cf`](https://github.com/NetworkManager/NetworkManager/commit/e93abf0552cf67c7fac5af9d365a833e747192f5)
* [`bac7a2821f3a`](https://github.com/NetworkManager/NetworkManager/commit/bac7a2821f3a52107f5c5dc447f337fdc56e3233)
* [`60cc501a665a`](https://github.com/NetworkManager/NetworkManager/commit/60cc501a665ad7fcfedcdba37557860458dca9a9)
* [`e7f3ccf7cdb5`](https://github.com/NetworkManager/NetworkManager/commit/e7f3ccf7cdb5b5d1765b434c71cf622a6a31a966)
* [`9ecdba316cf8`](https://github.com/NetworkManager/NetworkManager/commit/9ecdba316cf89612f3441aad16b99edc01c24e0d) Build the message without libnl object
* [`e9321713a90a`](https://github.com/NetworkManager/NetworkManager/commit/e9321713a90a2f1098afbdbbfbb462638c54a368) Avoid string copy
* [`75d694db9b7d`](https://github.com/NetworkManager/NetworkManager/commit/75d694db9b7db389904430eaaee4cfe3f9d592e9)
* [`53c69b1d6ec2`](https://github.com/NetworkManager/NetworkManager/commit/53c69b1d6ec29441649b42ed81169c26491ec26e)
* [`e17cd1d742c4`](https://github.com/NetworkManager/NetworkManager/commit/e17cd1d742c47122b159040ce23fbf684b21c486) Avoid string copy
* [`f17a20c568c7`](https://github.com/NetworkManager/NetworkManager/commit/f17a20c568c77c737a36aa908b3940d4bbfdf287)
* [`fedf7ca3034e`](https://github.com/NetworkManager/NetworkManager/commit/fedf7ca3034ed3708b8fb11d14e3e33b04bf28eb)
* [`c3d98a3df643`](https://github.com/NetworkManager/NetworkManager/commit/c3d98a3df6432f3d9d143dec6d328feec0873b4a)
* [`d0330bfa73da`](https://github.com/NetworkManager/NetworkManager/commit/d0330bfa73da84f3428682ad4caa6d81162c447e) Also memoization
* [`bafd0d557dd1`](https://github.com/NetworkManager/NetworkManager/commit/bafd0d557dd1ffbce9e1f7ce16c54d3bcbb42932)
* [`f8b74e19ea1e`](https://github.com/NetworkManager/NetworkManager/commit/f8b74e19ea1e01781d22fccc6e96129d88e6814e) Memoization when using new API
* [`5d1610b347ab`](https://github.com/NetworkManager/NetworkManager/commit/5d1610b347ab30e7bc028eabf6cd197bbda6f20b)
* [`bb273c0881b3`](https://github.com/NetworkManager/NetworkManager/commit/bb273c0881b39a1d930588014a2a0d2740272cfe)
* [`1fb3d5d7944e`](https://github.com/NetworkManager/NetworkManager/commit/1fb3d5d7944e20e3fd70296f763af5b23bb0efe1)
* [`e3d2fc861b86`](https://github.com/NetworkManager/NetworkManager/commit/e3d2fc861b86bbde2bfa3987e3f89a414046219a) Fixing branch condition to avoid heavy computation
* [`b58481b31ec5`](https://github.com/NetworkManager/NetworkManager/commit/b58481b31ec5785d208a7b11df5b05fc50c158e2)
* [`4417b8bf3eef`](https://github.com/NetworkManager/NetworkManager/commit/4417b8bf3eef8ca7cf45dc973ab107249bd10d67)
* [`46b452eb5a1c`](https://github.com/NetworkManager/NetworkManager/commit/46b452eb5a1c680646cf93a9f53c456426676e87)
* [`640736ff6505`](https://github.com/NetworkManager/NetworkManager/commit/640736ff65059bbe29ae7fa273015d849c78092a)
* [`142d83b01973`](https://github.com/NetworkManager/NetworkManager/commit/142d83b01973b843ea2eba38d0c19eee2c8dfbb6)
* [`470bcefa5f25`](https://github.com/NetworkManager/NetworkManager/commit/470bcefa5f259cb752660ac0069afe22dee8052e)
* [`c40acf334e90`](https://github.com/NetworkManager/NetworkManager/commit/c40acf334e90e77df5f6a0641d378812b552d518) Use union
* [`d7b1a911d913`](https://github.com/NetworkManager/NetworkManager/commit/d7b1a911d913a2b6f5b26bcab01965e3a533081d)
* [`79458a558bdf`](https://github.com/NetworkManager/NetworkManager/commit/79458a558bdf45a789df3024f84942f85eb15875)
* [`9440eefb6dc4`](https://github.com/NetworkManager/NetworkManager/commit/9440eefb6dc4939752bf049d1669a0a4d37213c2)
* [`04c70c76bccf`](https://github.com/NetworkManager/NetworkManager/commit/04c70c76bccf86a5870d957c35d4cff69ae5e558)
* [`2b43ce3a9406`](https://github.com/NetworkManager/NetworkManager/commit/2b43ce3a940664a063f49c167abb614c241940cf) Pass `enum` into the affected function
* [`0861f47a1c5d`](https://github.com/NetworkManager/NetworkManager/commit/0861f47a1c5d277188c31d3f5275755f830b0765) Data structure
* [`59f2c0fb3e9a`](https://github.com/NetworkManager/NetworkManager/commit/59f2c0fb3e9a7266ff39f4e1212024e9d845be75) Data structure
* [`f99723eda551`](https://github.com/NetworkManager/NetworkManager/commit/f99723eda5512bdf7f45cb583c21fc1e9ebf3e79) Data structure
* [`38273a88712c`](https://github.com/NetworkManager/NetworkManager/commit/38273a88712c8841a4d9619f69e522120e2b04b3) Data structure (inheritance -> plain class)
* [`6a42e18d53ea`](https://github.com/NetworkManager/NetworkManager/commit/6a42e18d53eaa8a643a5082041ecc3f4849f6192) Data structure
* [`36d7a3cf21ab`](https://github.com/NetworkManager/NetworkManager/commit/36d7a3cf21ab8b3f0ee5436eee382bf2577adb8a) Data structure
* [`22edeb5b691b`](https://github.com/NetworkManager/NetworkManager/commit/22edeb5b691befd796c534cf71901b32f0b7945b) Data structure
* [`4be4a3c21f63`](https://github.com/NetworkManager/NetworkManager/commit/4be4a3c21f63c53db4cd8ef02cd9e05c86f11b20) Data structure
* [`01a20015e0bf`](https://github.com/NetworkManager/NetworkManager/commit/01a20015e0bfd6d7c9aa872cfa31c4642ccb6825) Async
* [`32dfa563d1ea`](https://github.com/NetworkManager/NetworkManager/commit/32dfa563d1eaf4eea1245d26ac02b035a0a38605) Async
* [`1a070f6a44a0`](https://github.com/NetworkManager/NetworkManager/commit/1a070f6a44a0f1a39c174425be96dd8834bea25e) Static checking instead of dynamic checking
* [`baa0d95c95c4`](https://github.com/NetworkManager/NetworkManager/commit/baa0d95c95c48ed1b46464e1da4e369e5cf0951a) Static checking instead of dynamic checking
* [`b957403efd53`](https://github.com/NetworkManager/NetworkManager/commit/b957403efd53ff7d826ac7a4f80487032c03824b) Refactor to iterate the list at most twice
* [`ba1cc6a28803`](https://github.com/NetworkManager/NetworkManager/commit/ba1cc6a28803114016fab6dca35bac988da57ce3) Refactor to iterate the list at most twice
* [`62a4f2652f6a`](https://github.com/NetworkManager/NetworkManager/commit/62a4f2652f6a6a4ec4fa42260b4cb40e38752a0f) `broadcastAll` instead of `notifySingle`
* [`bfe66fb8f410`](https://github.com/NetworkManager/NetworkManager/commit/bfe66fb8f410a003f4d2ae05cba9baff798ddbad) Static checking instead of dynamic checking
* [`4dd6ab8f4b47`](https://github.com/NetworkManager/NetworkManager/commit/4dd6ab8f4b47e906be84442be8d68e82e41a1a24) Algorithm
* [`92712d3b31b5`](https://github.com/NetworkManager/NetworkManager/commit/92712d3b31b5e63e8c3c09b8cbcd05764a7a924e) Allow compiler optimization
* [`7e79e913ad11`](https://github.com/NetworkManager/NetworkManager/commit/7e79e913ad11e2c9247e840cd5cc5b2eccce25ab) Faster fallback
* [`ee76b0979ff0`](https://github.com/NetworkManager/NetworkManager/commit/ee76b0979ff0b3eef77168af0cbbdb4f1a9b843a) Algorithm
* [`9f79ae685d1b`](https://github.com/NetworkManager/NetworkManager/commit/9f79ae685d1ba2c05d1d0ac67767da095fbe0353)
* [`6ce7b7df96e5`](https://github.com/NetworkManager/NetworkManager/commit/6ce7b7df96e5a53b4e43a687dfedd8ae8a383fa0) Algorithm
* [`7860ef959ae4`](https://github.com/NetworkManager/NetworkManager/commit/7860ef959ae4b244017b9dc491e9c0a6024279dd) Compiler
* [`584a06e4e8e0`](https://github.com/NetworkManager/NetworkManager/commit/584a06e4e8e001c23c6590553bc01e313699ac39) Algorithm

## pulseaudio

* [`c82e4913e866`](https://github.com/pulseaudio/pulseaudio/commit/c82e4913e8666a47f11f91a313075ecdd7163d5a)
* [`a388b909f3a0`](https://github.com/pulseaudio/pulseaudio/commit/a388b909f3a0970cf56d4007509dd1b96b4265e2) Use more efficient mapping when possible
* [`555388ebba9a`](https://github.com/pulseaudio/pulseaudio/commit/555388ebba9afdc3db92dc221593cb06235f8ff0) Use more efficient mapping when possible
* [`9c4dcffca5c7`](https://github.com/pulseaudio/pulseaudio/commit/9c4dcffca5c7b31d7ff4ce77146ad87559bf70ef) Avoid waking the process up prematurely
* [`8fa81a93c984`](https://github.com/pulseaudio/pulseaudio/commit/8fa81a93c984523bf2bd5ad4b079bce8d14a9a4e)
* [`fe455ae0137c`](https://github.com/pulseaudio/pulseaudio/commit/fe455ae0137c44416b5f46a87abf33c05f513375) Split the function for path selection
* [`95b64804ab9d`](https://github.com/pulseaudio/pulseaudio/commit/95b64804ab9d21c6807c25eb3f03afd6bfd35cb6) Split the function for path selection
* [`78df02dba61d`](https://github.com/pulseaudio/pulseaudio/commit/78df02dba61d4d9e4f89225ddf69d5cfcdc9d184)
* [`192c3aaef835`](https://github.com/pulseaudio/pulseaudio/commit/192c3aaef8352ba9504aee4e311a23f6162d39d8) Reduce the buffer to be sent
* [`f4ab8bd83530`](https://github.com/pulseaudio/pulseaudio/commit/f4ab8bd8353073dcebb1e4e9fd340b9999cc7e9a)
* [`e92e8b11f1f3`](https://github.com/pulseaudio/pulseaudio/commit/e92e8b11f1f3b8c3af232388a52bbd06dbaaae7f)
* [`556cdfa1902d`](https://github.com/pulseaudio/pulseaudio/commit/556cdfa1902d9b2b4022f5a6d51813bdf567b17d) Access the member directly
* [`1e4e586150b7`](https://github.com/pulseaudio/pulseaudio/commit/1e4e586150b78e1d3999b9bcafecf34363ffff97) Initialize hardware mix path

## grep
* [`b408bcee1e81`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=b408bcee1e81a2c4cc187d520b1fd5ae0da68f13) Use `memchr2()` to speed up `memchr_kwset()`, while in `acexec_trans()` use `memchr_kwset()` when possible
* [`6e319a818ed7`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=6e319a818ed7b15b452ed2baab2f6a38d42fd1fe) C won't check the rhs of a logic and operator if the lhs is false. Therefore in this patch we can skip the slow old libc implementation of `mbrtowc()`
* [`0ffd7d26dc8f`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=0ffd7d26dc8f7627d2b4bd745ede7049a09b0dd9) Posing fewer condition on fast path
* [`f07b1b95dfc4`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=f07b1b95dfc47ab9cc492f07aa485e4ffb1cb9dc) minimize the usage of `memchr2()`
* [`296f3b994d59`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=296f3b994d59006a64fcd1a2398a6b3e13e84e3c) Avoid memory allocation
* [`290ca116c917`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=290ca116c9172d97b2b026951fac722d3bd3ced9) Add `try_fgrep_pattern()` to do fast conversion of keys
* [`2a45b2fbe2ca`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=2a45b2fbe2ca6d2d6c161d1593511b92d9e4640e) Avoid redundant memory allocation
* [`d67b7488d4ba`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=d67b7488d4ba337b96a97ebc4a632cd1a9cab168) Avoid redundant memory allocation
* [`724ac5564bc8`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=724ac5564bc8431b28a37e556a2adf27d7b3c74d)
* [`d1160ec6d239`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=d1160ec6d239b2e0f20c2fb3395e3b70963bf916) Make unibyte locale encode errors to avoid heavy weight error reporting
* [`f6de00f6cec3`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=f6de00f6cec3831b8f334de7dbd1b59115627457) Skip the hole in the file (Solaris only)
* [`9fa500407137`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=9fa500407137f49f6edc3c6b4ee6c7096f0190c5) / [`16f6616acaa4`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=16f6616acaa4fcf44edbc3b56ca10fc06b07cf25) Skipping error bytes and quickly matching empty strings before entering heavy `pcre_exec()`
* [`a0404f7e4266`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=a0404f7e42668762757f5462d0137db2937f16ff)
* [`a0404f7e4266`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=a0404f7e42668762757f5462d0137db2937f16ff)
* [`7600fde5bcf5`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=7600fde5bcf50872a89d427fea4add1f590ada36)
* [`c7c8bcdefe7b`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=c7c8bcdefe7be5f59a242eea63df7f64eacb6a09)
* [`bb3d1acbea88`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=bb3d1acbea88ced0cf4a93baa4ee8f2862f9e1ac) Add fast-path for single character matching
* [`f6603c4e1e04`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=f6603c4e1e04dbb87a7232c4b44acc6afdf65fef)
* [`5700656ed2ec`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=5700656ed2ec6a93e0a5c825b445f639d21a0d6e) Treat "[x-x]" case as "[x]"
* [`d4c8de2f3e4f`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=d4c8de2f3e4f6dfaf964f04600d4bb37285d5623)
* [`8a33cdeba4ba`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=8a33cdeba4ba032f3d0efebf230b86971c27e158) Initialization within certain condition
* [`14892aa6e0c2`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=14892aa6e0c21f49e5ec6d203253074ef15fedb0) Avoid using `memchr()` when possible
* [`fdf9fadd0316`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=fdf9fadd03160680e4d3edc886b1e31e424b535f)
* [`4fa6f48b5732`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=4fa6f48b573267e758650e114ec158d97916411e)
* [`aaefbfa486df`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=aaefbfa486df2587e25c8b59d1cca5f2092e7c7a) Skip easy bytes
* [`b342369b8f6d`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=b342369b8f6d0bee5f7eda04e63b694dbccc912a) Avoid mem allocation until needed
* [`d6c8415e9c8c`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=d6c8415e9c8c03db4f062a05dc7e7507af350306)
* [`1e166fdaa699`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=1e166fdaa69902793057ed2a63408f51c3b4273b) Avoid duplicated operations on the same character
* [`ad468bbe3df0`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=ad468bbe3df027f29ecb236283084fb60b734f68)
* [`523e085b6a0e`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=523e085b6a0e6b8d19e912011dd62b15c05a14d7) Add a loop to retry dfa superset immediately after a match maximize the usage of the fastpath for multiline matches
* [`3255bc58e8fb`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=3255bc58e8fb2d98145dbb2dd17bae0a5e47a85e) `dfafast` defines how end value for the search is set
* [`960ad317db21`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=960ad317db21e781b04010f4128bb149273a3327) Avoid some copy operations
* [`5cb49d2f375f`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=5cb49d2f375f0606ac9d916af6024d4b92ba0786)
* [`aa1f107a1439`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=aa1f107a143900d2d9a9d8c3aa541671140315ba) Complicated optimization, where in the case of UTF8 the first byte may cause the slow build of DFA. Optimizing by early scan & cache
* [`192d61e2828e`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=192d61e2828e13c4a2f1a81cd128721a229c88f9) Skip unnecessary scans
* [`79d7918c889d`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=79d7918c889dea0d83c270cc1e8a9d8786b20102) Shortened previous word character matching search path
* [`225362ac3967`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=225362ac3967a6ca6343958d7ad395a3dee1b63c) Set flag to avoid data format check and make unknown format optimization working
* [`55a0c73874bc`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=55a0c73874bcfaa73948fd034fb34e117266d623) Invoke exec function with different arguments as some arguments cause the function slow
* [`561ffc0f203e`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=561ffc0f203ea663bb668ac9f6014c16f39fc7d9) Change the sequence of strings to be matched and checked against
* [`42b14d981975`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=42b14d981975f50ca949ffbc234fb798816ec22e)
* [`d795d6d194a5`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=d795d6d194a594792b2a5bb35527a84a484ebeea) Efficient `lseek` can be used to shorten the search process by skipping holes in the file
* [`cd493ae8c13f`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=cd493ae8c13fb0380ca8b17d00429a901a885dc7)
* [`29b36e655aca`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=29b36e655aca16c4a28a7d8179c3096ebecf9815) Use fastmap when "-i" is not specified
* [`61e0661e1b08`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=61e0661e1b086c8933197f498139db26f157b713)
* [`b58925630c90`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=b58925630c90b852fb82784fbdf9624fa2461ed6) Cache the states of `ANYCHAR`
* [`b75ce6f7c611`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=b75ce6f7c611cb98549dc736947198e812b587c4)
* [`28f6eed8db83`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=28f6eed8db830ec2a92a560ef25cc93c6361b12c)
* [`7c9d39eab21e`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=7c9d39eab21ef5dfccb9c1626f9a00a32e514ca0) Adding superset of an dfa and use it before doing the full match of the pattern. If the string prematches the superset, it must match the original dfa as well.
* [`41ac7a996473`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=41ac7a99647316b5ea77d70199282fa9bbd731d4) Const
* [`1a45e78b8759`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=1a45e78b8759073c6f184039324d5b4bf8ef8f95) Adding pure attribute for better compiler optimization
* [`256a4b494fe1`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=256a4b494fe1c48083ba73b4f62607234e4fefd5) Process the sequence of the default case in bulk
* [`7eb003e1625a`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=7eb003e1625a9c4bd268fc46bad04c03ef7f0035) inlining functions
* [`ed0c7d559baf`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=ed0c7d559baf344db2b887256ccae8843e99efa8) inlining functions
* [`b639643840ef`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=b639643840ef506594b6c46e5b24d9980a33e78e) Change fgrep and egrep to shell script
* [`67822cff1f9f`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=67822cff1f9f54b0de5f4645c76e0777359f0d44) Split function call for compiler optimization
* [`bc0732da3a2c`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=bc0732da3a2c636b87a0173c2129162dbd806037) / [`671698472dea`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=671698472dea3b5ba4bb71074aa01b8c48bdb6c0) Removing optimization that is rarely executed but still wasting time on branching
* [`954a3eb3d15f`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=954a3eb3d15fee91b5d78632c6f6a5ed5f93505f) Use ternary operator
* [`a3d42de483a2`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=a3d42de483a29c1da381f5c3dad87cbbc86a5c70) Use Aho's algorithm with efficient trie building function from the Commentz-Walter algorithm
* [`757381e58d66`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=757381e58d669729510a89c323d4698f9a81f6c0) Avoid duplicated matching section
* [`97318f5e59a1`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=97318f5e59a1ef6feb8a378434a00932a3fc1e0b) Convert `grep -i foobar file` into `grep [fF][oO][oO][bB][aA][rR]` to speed up matching
* [`4fa1971d98c7`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=4fa1971d98c79b56b466eff57117351dc395ee2a)
* [`4c57448135eb`](http://git.savannah.gnu.org/cgit/grep.git/commit/?id=4c57448135ebd424c15c774ffba8a187e32bc0a3) Adding new `memchr2` api and use it

## rsyslog
* [`59e58b5ec8f7`](https://github.com/rsyslog/rsyslog/commit/59e58b5ec8f708e26cc580f9191ad557ca65cfc4) Use a new API instead of polling the existing filer descriptors
* [`b640a84242e8`](https://github.com/rsyslog/rsyslog/commit/b640a84242e87b8df18b554deee29294d10d7645) use `poll()` instead of `select()`
* [`edb81c0283bb`](https://github.com/rsyslog/rsyslog/commit/edb81c0283bbef29f05817062478c0a9e769269c)
* [`d3ef5a7bde0e`](https://github.com/rsyslog/rsyslog/commit/d3ef5a7bde0e387f738aca4fbc4d02461f09b954) Use single string buffer to avoid dual buffer extra copying (while make memory accesses more cache efficient)
* [`b9dcd4e43426`](https://github.com/rsyslog/rsyslog/commit/b9dcd4e434265821695865e18e9ca14fdc51c3f1)
* [`4193dc495d9e`](https://github.com/rsyslog/rsyslog/commit/4193dc495d9e331f62fe318dc1a61fcf507dc173)
* [`a000bd661e04`](https://github.com/rsyslog/rsyslog/commit/a000bd661e0459524bd146a803e17df813350d92)
* [`fa8ff7601de0`](https://github.com/rsyslog/rsyslog/commit/fa8ff7601de0340fbc0d3892b418a5dbb32a6e24)
* [`39b35a32b4a4`](https://github.com/rsyslog/rsyslog/commit/39b35a32b4a45ccb6506e15b02780c30ca8c3973) Avoid `strcmp()` in some cases
* [`123143d0f1f5`](https://github.com/rsyslog/rsyslog/commit/123143d0f1f5e99b04f963c5d785a8f8918decdc) Avoid `isdigit()`
* [`bb9650eb583f`](https://github.com/rsyslog/rsyslog/commit/bb9650eb583fa5fce62543a437e46456701b7695)
* [`eb97d25219a2`](https://github.com/rsyslog/rsyslog/commit/eb97d25219a279daceca29c08e68c864b5629901)
* [`49d1203b3582`](https://github.com/rsyslog/rsyslog/commit/49d1203b3582f7d637af11a226386a4df186e687) Avoid using slow `iscntrl()`
* [`262d05999029`](https://github.com/rsyslog/rsyslog/commit/262d05999029892e0550459a6361e74fed5a15fc) Build the string without `snprintf()`
* [`8339d54ddcbc`](https://github.com/rsyslog/rsyslog/commit/8339d54ddcbc93771fb6eb550cbf2d9ade988fb8)
* [`d748ba250ea9`](https://github.com/rsyslog/rsyslog/commit/d748ba250ea9cf77866f50bbcc8bd1fd97ee9c72)
* [`f3106c538f04`](https://github.com/rsyslog/rsyslog/commit/f3106c538f04ada7d1d0b1d1117d8930b0ce12d0)
* [`7619ad2c20af`](https://github.com/rsyslog/rsyslog/commit/7619ad2c20aff6039bf32d46605c22b9454e9519)
* [`4af7e0616618`](https://github.com/rsyslog/rsyslog/commit/4af7e0616618118cc1ed7deba6a7fcefeb4b4c08)
* [`69e1be41ac23`](https://github.com/rsyslog/rsyslog/commit/69e1be41ac23b429d841f6b7de49c0de03e108e3) Set a hash for strings to speed up following processing
* [`80d0ad708bfe`](https://github.com/rsyslog/rsyslog/commit/80d0ad708bfe29fc255b60bda6a37df58737cb8f) Early conversion of string for faster processing
* [`6c52f29d593a`](https://github.com/rsyslog/rsyslog/commit/6c52f29d593a27f934a1871d40eed84ebde3f3a6) Reduce the size of buffer of message submission
* [`33fe6e484c8b`](https://github.com/rsyslog/rsyslog/commit/33fe6e484c8b3f8aeaad4a7e4c417ac837a81aac)
* [`eb5c7a041990`](https://github.com/rsyslog/rsyslog/commit/eb5c7a04199028703a328d199c36ac6f5b631ccd)
* [`62161ae32d46`](https://github.com/rsyslog/rsyslog/commit/62161ae32d46cd95d9b4974c32c0f4df00c9964a)
* [`023becabd706`](https://github.com/rsyslog/rsyslog/commit/023becabd706a7ac177470eec2d232c037a8a83b)
* [`f75395172eac`](https://github.com/rsyslog/rsyslog/commit/f75395172eac1dfcc63c06ec050739b952eb5cb8)
* [`440aaff11460`](https://github.com/rsyslog/rsyslog/commit/440aaff114607e82aefee56336846611a2e35c9c)
* [`0d71694fb3cb`](https://github.com/rsyslog/rsyslog/commit/0d71694fb3cbff71d504769e0e70a58ebe5f9a0d)
* [`855b68e1be65`](https://github.com/rsyslog/rsyslog/commit/855b68e1be652b4c51052969a9550d33b2aec612)
* [`beed1bda6969`](https://github.com/rsyslog/rsyslog/commit/beed1bda6969b70b608ff9e606f52ac6d41cc8c1)
* [`90e1478f11b7`](https://github.com/rsyslog/rsyslog/commit/90e1478f11b7a9d253899104b20900bd999970f1) Fast access
* [`01de5afe83d4`](https://github.com/rsyslog/rsyslog/commit/01de5afe83d4869fac34f26e56fc2ccfcc369b8f) Add const for compiler optimization
* [`567469f94ad5`](https://github.com/rsyslog/rsyslog/commit/567469f94ad540f14905c98a41cd16c10783b536)
* [`9273b4bb4dcb`](https://github.com/rsyslog/rsyslog/commit/9273b4bb4dcb6683cf6825fedd3cb5cd0f59805a) Change mem layout for CPU cache hit
* [`b76e48711fc0`](https://github.com/rsyslog/rsyslog/commit/b76e48711fc045241f8e7c5d1f1766172edafa04) Timer
* [`5ed3beb5698e`](https://github.com/rsyslog/rsyslog/commit/5ed3beb5698e7b2e2cb42777ce1f61a2489892da) make connection parallel
* [`ea1860deb070`](https://github.com/rsyslog/rsyslog/commit/ea1860deb0707844415160b8b315a35cea2b89b7)
* [`d741c3e218ac`](https://github.com/rsyslog/rsyslog/commit/d741c3e218acd9e965388df30da4f82cbee53028) Change of API used
* [`752d32f6005a`](https://github.com/rsyslog/rsyslog/commit/752d32f6005abe7d0bd1f406a30605abb7dc8beb) Change of API used
* [`447f92cc260b`](https://github.com/rsyslog/rsyslog/commit/447f92cc260bc79991437d553d0cad4f94fb8f51) Change of API used

## lvm2
* [`ada5733c5652`](https://github.com/lvmteam/lvm2/commit/ada5733c5652d456ffa138b0d07e6897824813b0) 'Unblocking' clear
* [`55ca8043d4bb`](https://github.com/lvmteam/lvm2/commit/55ca8043d4bb0ef4435a24728cad34f8b0ff6c8c)
* [`2d1dbb9eddd9`](https://github.com/lvmteam/lvm2/commit/2d1dbb9eddd9dbc3aedf4572926178e532899995)
* [`217f3f8741c1`](https://github.com/lvmteam/lvm2/commit/217f3f8741c1f516ab1f039b8725ac3ce638e302)
* [`c086dfadc389`](https://github.com/lvmteam/lvm2/commit/c086dfadc389551b9a2d7b4c26931e5e74ada8d6)
* [`6dd0bd0255d5`](https://github.com/lvmteam/lvm2/commit/6dd0bd0255d585feca61000432d2de9552ede4e0)
* [`e2c7e0ad1196`](https://github.com/lvmteam/lvm2/commit/e2c7e0ad1196cc9323b63807ed199bcf4b10807c)
* [`8e9d5d12ae3a`](https://github.com/lvmteam/lvm2/commit/8e9d5d12ae3ac75852ebd74b64c28e31abb68d0e) Avoid collocate when `pvmove()`
* [`598c82fc077c`](https://github.com/lvmteam/lvm2/commit/598c82fc077c8a3edfbdb664b41efd89d05b5a10)
* [`3b604e5c8e50`](https://github.com/lvmteam/lvm2/commit/3b604e5c8e505138fc86e009375fd33160ab84ff)
* [`9a060948242e`](https://github.com/lvmteam/lvm2/commit/9a060948242e830555436678f5b15b84bca70b04)
* [`65d0089c6477`](https://github.com/lvmteam/lvm2/commit/65d0089c6477e6bc30ef64ce9fd79e8c855c4529) Check whether the major device number is in the list instead of iterating through `sysfs`
* [`f65dd341a561`](https://github.com/lvmteam/lvm2/commit/f65dd341a5617aeffe18127ba5bd7ca5ca4d8477) Get VG metadata when it's synchronous with the data on disk
* [`2c4e8254de59`](https://github.com/lvmteam/lvm2/commit/2c4e8254de59579a84b6c1f3dcc66868b3e9ef34)
* [`9712995edd60`](https://github.com/lvmteam/lvm2/commit/9712995edd60220cd525c51b6bab9f7bd3b38b5a)
* [`99237f0908d8`](https://github.com/lvmteam/lvm2/commit/99237f0908d87592815f4bdf3c239e8a108e835c)
* [`18dfbbb150e0`](https://github.com/lvmteam/lvm2/commit/18dfbbb150e04026e4c11822e5f80f79da691d08)
* [`6a41286c012e`](https://github.com/lvmteam/lvm2/commit/6a41286c012e8a15a41f0d807c0cb62de7f515b3)
* [`4dc81848034f`](https://github.com/lvmteam/lvm2/commit/4dc81848034f4f5edb02335af03e50f45c3977f0)
* [`0912cf67aae2`](https://github.com/lvmteam/lvm2/commit/0912cf67aae21446a10b3a22067aee1e8f792856)
* [`96626f64fa49`](https://github.com/lvmteam/lvm2/commit/96626f64fa499fb45dfc1afb39b86b70b8d3d9a4)
* [`1ff7e214e0cf`](https://github.com/lvmteam/lvm2/commit/1ff7e214e0cf0e7ef0280b71ab128e029172a1d9)
* [`420af27f088a`](https://github.com/lvmteam/lvm2/commit/420af27f088a3808816ba3ab5b47dfa400fef308) hash table -> radix tree
* [`719261a33a34`](https://github.com/lvmteam/lvm2/commit/719261a33a34ff177767324089b6daa128c5c2f0) Read the first part of kmsg instead of the entire buffer
* [`0dc73f7dbd49`](https://github.com/lvmteam/lvm2/commit/0dc73f7dbd49308b1864444d1decfaf8e10d2d81) Const
* [`dd5d865020ac`](https://github.com/lvmteam/lvm2/commit/dd5d865020acd545712d4bcc0f3236143de4d76d) Fix hang by adding timeout
* [`5577f2f4f0a5`](https://github.com/lvmteam/lvm2/commit/5577f2f4f0a5518662b9cedb6a373ccb67a71917) || instead of |
* [`bfeabea63178`](https://github.com/lvmteam/lvm2/commit/bfeabea631782b3f0b8ec6494c4490663c412774) Avoid multiple metadata check, but the code still double checks
* [`576dd1fc4125`](https://github.com/lvmteam/lvm2/commit/576dd1fc41259a9ed3d95b2385305fc035058618)
* [`772834e40aef`](https://github.com/lvmteam/lvm2/commit/772834e40aef44a8f7bc884050e1b88087073706)

## llvm
* [`6c8d9bce0325`](https://github.com/llvm-mirror/llvm/commit/6c8d9bce0325cb812d70a7d444ddf9aa38859be7)
* [`1610bcf8d924`](https://github.com/llvm-mirror/llvm/commit/1610bcf8d92427d2c4cc34c632f3525a2b153e65)
* [`bf0b5f5f3305`](https://github.com/llvm-mirror/llvm/commit/bf0b5f5f3305101e3fc80ede1777c7648d4600a4) Static header selection
* [`549896a73688`](https://github.com/llvm-mirror/llvm/commit/549896a7368869a9f93b59071a5bcf11b2383a88)
* [`ffc893deb710`](https://github.com/llvm-mirror/llvm/commit/ffc893deb7106ee0c541b44ef41506a32e1e8350)
* [`95d021ba685c`](https://github.com/llvm-mirror/llvm/commit/95d021ba685c54ba516442811f5926a4f6d2cf9a)
* [`092e5e75661f`](https://github.com/llvm-mirror/llvm/commit/092e5e75661fdd5d54a748fb00fab59d21031268)
* [`e4f44dfa7398`](https://github.com/llvm-mirror/llvm/commit/e4f44dfa7398c7669169973c1509659c5af60108)
* [`22e14e2efdc3`](https://github.com/llvm-mirror/llvm/commit/22e14e2efdc3bf38c9b75c6694945f98b3dac96b)
* [`04a303b8214f`](https://github.com/llvm-mirror/llvm/commit/04a303b8214f125f8805c0df5b60555349c28786) Avoid copy
* [`16bb45c5c8e9`](https://github.com/llvm-mirror/llvm/commit/16bb45c5c8e918efa732fd7d0b31c0f31dc2a979)
* [`b0edfb8160e3`](https://github.com/llvm-mirror/llvm/commit/b0edfb8160e3c836faccae1945e2c49db3701683)
* [`2638e45e83af`](https://github.com/llvm-mirror/llvm/commit/2638e45e83af8ae75fcb5ad2ac89feb8a0c1ff99) Specialization of a generic type to avoid buffer creation
* [`b6b25740b763`](https://github.com/llvm-mirror/llvm/commit/b6b25740b763ba8cf7d6eecb59b47e25d6edc1f5) Unmerge GEP to reduce live values and register pressure
* [`16cba6923a59`](https://github.com/llvm-mirror/llvm/commit/16cba6923a59f37985c34484a64879b7bca263bd)
* [`c0f82e4a5178`](https://github.com/llvm-mirror/llvm/commit/c0f82e4a517806d177d696b725025cbc6084b1ab)
* [`1a8392e4835e`](https://github.com/llvm-mirror/llvm/commit/1a8392e4835e4843c7a473080610175c2230534b)
* [`7e9986fa34a9`](https://github.com/llvm-mirror/llvm/commit/7e9986fa34a97b6d7ae4102de57baa9f0b7b0e0f)
* [`1e0b73ce8bf5`](https://github.com/llvm-mirror/llvm/commit/1e0b73ce8bf57971df1dc122e2b36352e084d201) Remove unreachable blocks before optimization
* [`9f299abc0567`](https://github.com/llvm-mirror/llvm/commit/9f299abc056749f22fa2f18ce217b1ba5036f0fc) Drop unused table
* [`baf868b9b8d1`](https://github.com/llvm-mirror/llvm/commit/baf868b9b8d187744d183d57ef3cbb2a44ca047a)
* [`b6f73f8f0bee`](https://github.com/llvm-mirror/llvm/commit/b6f73f8f0beec41e5d2a29034b028bab3f22080f) Use `SmallMapVector`
* [`f28cb39e4ca0`](https://github.com/llvm-mirror/llvm/commit/f28cb39e4ca07c387dd270ce123753f898a75d5c) Use maps for efficient iteration
* [`427afb91d382`](https://github.com/llvm-mirror/llvm/commit/427afb91d38252b1abf0afaa08099f1f8cda3b66) `FnSet` -> `FnTree`
* [`e0714cfc0fec`](https://github.com/llvm-mirror/llvm/commit/e0714cfc0fecd4a47349ffc676f1d91c03eb4e1c) Const vs non-const
* [`c4fbd3fd6268`](https://github.com/llvm-mirror/llvm/commit/c4fbd3fd6268eb46072160a519fa6929929e2677) Array accesses faster than `switch case`
* [`8c955ea858b0`](https://github.com/llvm-mirror/llvm/commit/8c955ea858b0c99c856c7c10a3eee7576d13abd1) Using `FoldingSet`
* [`c5374d2ba55e`](https://github.com/llvm-mirror/llvm/commit/c5374d2ba55eb0b28ae250d044a652c09b15d870) Iterating to avoid hashing/lookups
* [`f4c3a5601aef`](https://github.com/llvm-mirror/llvm/commit/f4c3a5601aefe2e6500ff4af394196da55203e52)
* [`ab1f4ef9a2b7`](https://github.com/llvm-mirror/llvm/commit/ab1f4ef9a2b7200c20a98fe9882656752f4452d5) Pass choosing algorithm
* [`3d019d384a40`](https://github.com/llvm-mirror/llvm/commit/3d019d384a400c6ef66164dcb3abe4d8d846308f) Algo
* [`a9fbbb48bcc5`](https://github.com/llvm-mirror/llvm/commit/a9fbbb48bcc56354681fc2ead593a9861bfec699)
* [`9ece8eb94564`](https://github.com/llvm-mirror/llvm/commit/9ece8eb9456421244dbf894676848574f21b401f) Partial specialization of the more efficient implementation
* [`1ecf0813db3b`](https://github.com/llvm-mirror/llvm/commit/1ecf0813db3b6e9a13d05fa891023ffef6b3abf9)

## git

* [`252d079cbd27`](https://github.com/git/git/commit/252d079cbd27ca442d94535e3979145eceaf082b)
* [`1a2e1a76ec2c`](https://github.com/git/git/commit/1a2e1a76ec2cbbafe60ffd124f673f62045fb0d3)
* [`9a48a615b47d`](https://github.com/git/git/commit/9a48a615b47d940764e00c872ddbae601a1e5f8a)
* [`4e38e9b1d05f`](https://github.com/git/git/commit/4e38e9b1d05fb00ad87cba8c44117a537d779c9e)
* [`0cacebf099dd`](https://github.com/git/git/commit/0cacebf099dd6467845419f212acdcfe5f8d923f)
* [`5ebd9472a490`](https://github.com/git/git/commit/5ebd9472a490b12ff941b8085b1b0932a755ffcc)
* [`f23092f19e73`](https://github.com/git/git/commit/f23092f19e73f5d8c3480ef02104af627a90361f)
* [`163ee5e63523`](https://github.com/git/git/commit/163ee5e635233f8614c29c11c9b8ee02902d65c4)
* [`176841f0c9b4`](https://github.com/git/git/commit/176841f0c9b470b008c95eb50b7bb9424321d540)
* [`af952dac7a69`](https://github.com/git/git/commit/af952dac7a6997e587ded97e4051a927e5384423)
* [`a6eec1263883`](https://github.com/git/git/commit/a6eec1263883ce9787a354e1635b7b732e72c3c9)
* [`e5494631ed94`](https://github.com/git/git/commit/e5494631ed94017da862d55eb6393a0d01d8b91d)
* [`9bc4222746a0`](https://github.com/git/git/commit/9bc4222746a09d57998903dab0361b1d7adc271f)
* [`7eb6e10c9d7f`](https://github.com/git/git/commit/7eb6e10c9d7f43913615667740d1b22055cfba1f)
* [`6df5762db354`](https://github.com/git/git/commit/6df5762db354ca55a0cf77451d06b332b7de0b82)
* [`dbbc93b221c6`](https://github.com/git/git/commit/dbbc93b221c6ee9cb2d417a43078b0d2a986fd33)
* [`8d6a7e9a1984`](https://github.com/git/git/commit/8d6a7e9a198488a1594cd178240fb56085129c78)
* [`4e1d1a2eea25`](https://github.com/git/git/commit/4e1d1a2eea25878a2128e376bff8b4a1b2216b15)
* [`2dacf26d0985`](https://github.com/git/git/commit/2dacf26d0985521c0f30e535963a45257b63ea21)
* [`0e87b85683df`](https://github.com/git/git/commit/0e87b85683df145007862d23b5f0773368d66464)
* [`f35650dff6a4`](https://github.com/git/git/commit/f35650dff6a4500e317803165b13cc087f48ee85)
* [`c08e4d5b5cfa`](https://github.com/git/git/commit/c08e4d5b5cfa9f692cb8c81e5e9615e330f299c2)
* [`e70a3030e747`](https://github.com/git/git/commit/e70a3030e747312327a4f3619247bf8a986aa577)
* [`3bc7a05b1a78`](https://github.com/git/git/commit/3bc7a05b1a78b850da94ca85267ca279489ce70f)
* [`990a4fea9698`](https://github.com/git/git/commit/990a4fea96983d54c0dcc96352e4c86404eceb77) Add fast path for non-wild match
* [`12cd81743dc4`](https://github.com/git/git/commit/12cd81743dc4645ef909b0c38582f5714c9a8ff7)
* [`832258da969f`](https://github.com/git/git/commit/832258da969fbedbbd1d474900dbdbdf23d4bca1) Limit buggy slow part in certain cases
* [`9831e92bfa83`](https://github.com/git/git/commit/9831e92bfa833ee9c0ce464bbc2f941ae6c2698d) Remove the slow API
* [`b7099a06e8ff`](https://github.com/git/git/commit/b7099a06e8ffe61a06d3e32632e832e59f23bd4d)
* [`1974f4791af2`](https://github.com/git/git/commit/1974f4791af2b8720f2983149defe13e571b3884) Performance improvement when tracing is disabled
* [`6cbc478d83b5`](https://github.com/git/git/commit/6cbc478d83b5773d1925869e50bf6067306f4817)
* [`2523c4be855a`](https://github.com/git/git/commit/2523c4be855af84460e70ab5c8375534f8cefed5)
* [`3ecc7040eff2`](https://github.com/git/git/commit/3ecc7040eff29fea0051df9faf21b0a73ee6d911)
* [`ee7fd70edfc5`](https://github.com/git/git/commit/ee7fd70edfc51ec88865c573ff950b4dfbef1833)
* [`3446a59b3950`](https://github.com/git/git/commit/3446a59b3950d57960e27f8a2c7e41462bd2bcf4)
* [`7a40a95eb4f7`](https://github.com/git/git/commit/7a40a95eb4f79517750eb2bcd81342c25c6db406)
* [`0952ca8a959e`](https://github.com/git/git/commit/0952ca8a959ec3ecd8e929592249732e619416e3) Fastfail
* [`6bf3b813486b`](https://github.com/git/git/commit/6bf3b813486b4528feca39d599c256f662defc14) Skip diff large files
* [`645c432d6196`](https://github.com/git/git/commit/645c432d6196903bcd9e8a82d150aac1428ccf34)
* [`6a36e1e7bb64`](https://github.com/git/git/commit/6a36e1e7bb64726cc712259aff57179d81361b5d)
* [`31471ba21ee2`](https://github.com/git/git/commit/31471ba21ee29886ab856981e52f723c913d7f40)
* [`c6807a40dcd2`](https://github.com/git/git/commit/c6807a40dcd29f7e5ad1e2f4fc44f1729c9afa11)
* [`9ec726a4120b`](https://github.com/git/git/commit/9ec726a4120bb219530faf988198a704ec7dd1f1)
* [`adcc94a0aa70`](https://github.com/git/git/commit/adcc94a0aa7055be4133ebda8b25f4af63285c6d) Add cases for fast-forward
* [`4c30d50402c1`](https://github.com/git/git/commit/4c30d50402c17d2569151820b92cea110ad1d240)
* [`108f530385e9`](https://github.com/git/git/commit/108f530385e969feab343b2b8acadeb7bb670009)
* [`9a9f243f6426`](https://github.com/git/git/commit/9a9f243f6426056058c7c29999c27a086f94e035)
* [`ca598d5f2ab9`](https://github.com/git/git/commit/ca598d5f2ab988935a5b882b44122cbfa5fd99f5)
* [`ad635e82d600`](https://github.com/git/git/commit/ad635e82d600e1b725a2e65d69114140db6bc876)
* [`da470981defc`](https://github.com/git/git/commit/da470981defcace6e909b74ebc4ab5a40a702728)
* [`5c9f9bf313e8`](https://github.com/git/git/commit/5c9f9bf313e872934c9b4563b7b0bcdedb39eb86)
* [`38ccaf93bbf5`](https://github.com/git/git/commit/38ccaf93bbf5a99dbff908068292ffaa5bafe25e)
* [`df08eb357dd7`](https://github.com/git/git/commit/df08eb357dd7f432c3dcbe0ef4b3212a38b4aeff)
* [`846df809bc67`](https://github.com/git/git/commit/846df809bc671d6487fa2be7550e3ed2b62e62f9)
* [`f5b2dec1657e`](https://github.com/git/git/commit/f5b2dec1657e09a22f8b2aefa25d022988e3e467)
* [`41a078c60b82`](https://github.com/git/git/commit/41a078c60b82bad4edf9d1bd8e826aae5f020ee5)
* [`a849735bfbf1`](https://github.com/git/git/commit/a849735bfbf159b98ead9ef4c843dc8acfd372f0)
* [`abb4bb83845d`](https://github.com/git/git/commit/abb4bb83845dc012ffe1c04750d1a09edd598a82)
* [`b4da37380b77`](https://github.com/git/git/commit/b4da37380b7774248086f42bcd59397a44e1ac79)
* [`d8febde370aa`](https://github.com/git/git/commit/d8febde370aa464a5208cf094f306343e4ecb6dc) Use tree structure
* [`0f9e62e0847c`](https://github.com/git/git/commit/0f9e62e0847c075678a7a5a748567d1e881d16f8) Use bitmap to avoid traversal
* [`9a414486d9f0`](https://github.com/git/git/commit/9a414486d9f0325c3663210471e4673ed0cd862c) Move the found item to the head of the collision list
* [`3fb9d685dbff`](https://github.com/git/git/commit/3fb9d685dbff20bbc9d441a85eb3bef17ef5ec0f) Change the type of parameters
* [`c44a4c650c66`](https://github.com/git/git/commit/c44a4c650c66eb7b8d50c57fd4e1bff1add7bf77)
* [`82d0a8c05af8`](https://github.com/git/git/commit/82d0a8c05af8d1f7df0c308d23d078e0bac9c594)
* [`62db5247790f`](https://github.com/git/git/commit/62db5247790f2612c0b407a15d1901d88789d35a) Work in plain C instead of shell script
* [`144e4cf7092e`](https://github.com/git/git/commit/144e4cf7092ee8cff44e9c7600aaa7515ad6a78f)
* [`7e6ac6e4391c`](https://github.com/git/git/commit/7e6ac6e4391caa0fc379cb699013d503380e4214) Algorithm
* [`36f0f344e71b`](https://github.com/git/git/commit/36f0f344e71b928d47871e39fcef1908cb47c47b) Repack files for better performance
* [`a7ddaa8eacb4`](https://github.com/git/git/commit/a7ddaa8eacb45fdd5241e52d72e6f75d8b67b953) ???
* [`d0da003d5b1e`](https://github.com/git/git/commit/d0da003d5b1e65f6e52920e42582f43b357782ee)
* [`5d19e8138d5f`](https://github.com/git/git/commit/5d19e8138d5f24dbbe8d3736b4e57e117fc099b3) Update promisor to do repack on these files
* [`d5ca1ab395d4`](https://github.com/git/git/commit/d5ca1ab395d4994346e13569ad7a7f5cd4acfade)
* [`6f92e5ff3cdc`](https://github.com/git/git/commit/6f92e5ff3cdc813de8ef5327fd4bad492fb7d6c9) ???
* [`7b64d42d2220`](https://github.com/git/git/commit/7b64d42d22206d9995a8f0cb3b515e623cac4702) Add string interning

## clang

* [`e86ee1a213d2`](https://github.com/llvm-mirror/clang/commit/e86ee1a213d244bb66b7eef3e9ab2266908cf4af)
* [`d0d4d66cd1f7`](https://github.com/llvm-mirror/clang/commit/d0d4d66cd1f75aa7be820525088d566592efe71d) Use tooling instance instead of invoking the compiler externally
* [`9ac438062906`](https://github.com/llvm-mirror/clang/commit/9ac438062906eff96a8f2cf1dd61dd630645717a) Quicker accesses to some data
* [`df5f80f8a34e`](https://github.com/llvm-mirror/clang/commit/df5f80f8a34e26a4fb77f48f858c7838426a0785)
* [`4ed69c25ab95`](https://github.com/llvm-mirror/clang/commit/4ed69c25ab95ef923b9917a3c3599665ca7bbda7) Provides fast yet inaccurate declaration in libclang
* [`0a7a42e99096`](https://github.com/llvm-mirror/clang/commit/0a7a42e99096b810ccdb4b21d72b9d711996afd2)
* [`baa7ca114299`](https://github.com/llvm-mirror/clang/commit/baa7ca1142990e1ad6d4e9d2c73adb749ff50789)
* [`ceadeb9d7861`](https://github.com/llvm-mirror/clang/commit/ceadeb9d7861b4d48124b8fb0d2a77eed159d64c)
* [`951094baa05a`](https://github.com/llvm-mirror/clang/commit/951094baa05a72f2623b7e25df609796ad97d908)
* [`55b2d89ef0e2`](https://github.com/llvm-mirror/clang/commit/55b2d89ef0e286fa369cd6af5e6c64d8e8d58b87) Use a unified function instead of fast/slow path
* [`3c02b7312f41`](https://github.com/llvm-mirror/clang/commit/3c02b7312f417e6a400c8aaba97b167e36c6e410)
* [`5a04f9fc2b00`](https://github.com/llvm-mirror/clang/commit/5a04f9fc2b000da98fd903c8156034304bdadb2f)
* [`85fb4f5d7576`](https://github.com/llvm-mirror/clang/commit/85fb4f5d7576299806b785a2347ef834cb47372b) Fail fast instead of rollback
* [`562d45ce29e8`](https://github.com/llvm-mirror/clang/commit/562d45ce29e8904e686e4caf18b8e6c4d6d3798d)
* [`575c8ea65c2a`](https://github.com/llvm-mirror/clang/commit/575c8ea65c2a5212d95e9881f8a2aac27315dd53)
* [`174cfde2479e`](https://github.com/llvm-mirror/clang/commit/174cfde2479e0e3c481ca1361dd6960f3a99e75d) Use more efficient API
* [`f20a280b3ba3`](https://github.com/llvm-mirror/clang/commit/f20a280b3ba367e83e1c21cfd4366cd55b8d20cb)
* [`d0d4d66cd1f7`](https://github.com/llvm-mirror/clang/commit/d0d4d66cd1f75aa7be820525088d566592efe71d) Do not start the frontend from driver
* [`26e65cec83b8`](https://github.com/llvm-mirror/clang/commit/26e65cec83b81d60dd4bc1977f01280905f00edc)
* [`fb5f718cf1ca`](https://github.com/llvm-mirror/clang/commit/fb5f718cf1cae842bad8098764204e5a5b06a2d9) Use `bzero()` when possible
* [`4a18c3b25a2d`](https://github.com/llvm-mirror/clang/commit/4a18c3b25a2d7eb7f770ce91ee5e14433b2a1cb6) Only scan the entry list
* [`3ed5b235b114`](https://github.com/llvm-mirror/clang/commit/3ed5b235b1148ad0c5bf9bc69c5bb92a1859eea8)
* [`9d33e30aba42`](https://github.com/llvm-mirror/clang/commit/9d33e30aba42a968017b1bfe51951337322f06eb)
* [`89aa3ede8e97`](https://github.com/llvm-mirror/clang/commit/89aa3ede8e973571becdb6314a861c6d2fc0a39f) Fail fast instead of rollback
* [`72a418772e26`](https://github.com/llvm-mirror/clang/commit/72a418772e26e11a034affc9604a68b180f48eec)
* [`f78232dcbf73`](https://github.com/llvm-mirror/clang/commit/f78232dcbf734a1c7e42ae60b58c9bbe14b64516)
* [`007983114f44`](https://github.com/llvm-mirror/clang/commit/007983114f448135906abddea70250d463fcd13a)
* [`7cea14876025`](https://github.com/llvm-mirror/clang/commit/7cea1487602536e91a2c36511f5ad56ff1b2dc68)
* [`aa945900d543`](https://github.com/llvm-mirror/clang/commit/aa945900d5438984bdcaac85c4f54868292231f4)
* [`3ee4cc4acfa6`](https://github.com/llvm-mirror/clang/commit/3ee4cc4acfa675c8cace6b05f2b41ed45a6a1033)
* [`41748f7d3ae3`](https://github.com/llvm-mirror/clang/commit/41748f7d3ae36db1b1c52eaaf76631ed60c79c53)
* [`0eaba5f9b9a6`](https://github.com/llvm-mirror/clang/commit/0eaba5f9b9a6e7b9dd674958a557b39bd8a67c22)
* [`29e517e070cc`](https://github.com/llvm-mirror/clang/commit/29e517e070ccd4d79781f5f92f43f8dec6447438)
* [`d07865b42dcb`](https://github.com/llvm-mirror/clang/commit/d07865b42dcb32154c75134fded51b38cc55a0c4) Precomputation
* [`3548068c22f8`](https://github.com/llvm-mirror/clang/commit/3548068c22f809e5bc64b83d2c3622018469256c)
* [`e7a77727804c`](https://github.com/llvm-mirror/clang/commit/e7a77727804c12750cb39e8732e26f2a26e4ce0f)
* [`ac0a256e8571`](https://github.com/llvm-mirror/clang/commit/ac0a256e8571131b60958cc8ce01a53c9f56d180)
* [`4c3cdee2a504`](https://github.com/llvm-mirror/clang/commit/4c3cdee2a5047d05201424bf9f80b6664619550d) Not a simple `cache = computation();` fix
* [`9bdffb989936`](https://github.com/llvm-mirror/clang/commit/9bdffb9899360b2598c110ebfcdc685f76c3ac50)
* [`a6b00fc97669`](https://github.com/llvm-mirror/clang/commit/a6b00fc97669aa25d89ae9f202b05dfadfd0e324) a new class caching identifier information of each module
* [`039fc1ebe202`](https://github.com/llvm-mirror/clang/commit/039fc1ebe2026d1b275bbfd391af149db30a54f3) Complicated refactoring that causes duplicated traversals over arguments, which is then optimized by memorizing call arguments and IR arguments
* [`ccefdc8cb94e`](https://github.com/llvm-mirror/clang/commit/ccefdc8cb94ecb4121695b30c2e2d0054d6b7ded)
* [`4411b423e91d`](https://github.com/llvm-mirror/clang/commit/4411b423e91da0a2c879b70c0222aeba35f72044)
* [`f4f66331f0df`](https://github.com/llvm-mirror/clang/commit/f4f66331f0df6dbacebaf95b03fcfebc95970151)
* [`926f58b96ef5`](https://github.com/llvm-mirror/clang/commit/926f58b96ef5cea00290671da575b7d2273c2c5f)
* [`953de1f092f5`](https://github.com/llvm-mirror/clang/commit/953de1f092f5af5f0a1d399bd5c3f51a84804609) Using `enum` instead of a function pointer
* [`499fc534bd02`](https://github.com/llvm-mirror/clang/commit/499fc534bd0210b73176bb9bc0ffbe6e59512133) Change sema map to extern C decls for lookup
* [`fae6a43ca3b1`](https://github.com/llvm-mirror/clang/commit/fae6a43ca3b185e456ad8c478f913cf83b36908a) Use another data structure
* [`90cdec1762c2`](https://github.com/llvm-mirror/clang/commit/90cdec1762c2a72b7c63f67c025597ab9ac8cae0)
* [`90ea10e5ea2f`](https://github.com/llvm-mirror/clang/commit/90ea10e5ea2f9907adea9f90667c34cc39820e61) Compute the json object without checking against raw json strings
* [`dfa97d29ffee`](https://github.com/llvm-mirror/clang/commit/dfa97d29ffeeb84a150671286690bfe0fe20c6f9) / [`cf9fe978839a`](https://github.com/llvm-mirror/clang/commit/cf9fe978839aa983f5bd41a0987f5a8eea1fad87) / [`329e886c49d0`](https://github.com/llvm-mirror/clang/commit/329e886c49d0d592d7a99c69bfb07791e44bcd61) Create VFS overlay for replay lookup
* [`bdb673743a14`](https://github.com/llvm-mirror/clang/commit/bdb673743a14ba6d761febf41176581d7674df34)
* [`211035090970`](https://github.com/llvm-mirror/clang/commit/2110350909701fcd6b55c636e24a675f0a905fea) / [`a5f80b2ea6d3`](https://github.com/llvm-mirror/clang/commit/a5f80b2ea6d30c5055c067530d63bb0dcaf937d0) Shortest path finding
* [`ac39f135ea0d`](https://github.com/llvm-mirror/clang/commit/ac39f135ea0d45bc37d1154fa8380d46aac0547d) Unknown if it's performance related
* [`a19dc41bd408`](https://github.com/llvm-mirror/clang/commit/a19dc41bd408732d407d0152f67b389f7333db25) ???
* [`3a67a19e28fc`](https://github.com/llvm-mirror/clang/commit/3a67a19e28fc3380fb836a5ff9a33a55cf197547) Moving information into a container
* [`1c11b7cda0af`](https://github.com/llvm-mirror/clang/commit/1c11b7cda0af21cf2c52fdaa4392977df85a1a9c)
* [`2f13eb116e62`](https://github.com/llvm-mirror/clang/commit/2f13eb116e62161c5e4d198f7831f226e5cea9da) Make recursive iterative
* [`0d816739a82d`](https://github.com/llvm-mirror/clang/commit/0d816739a82da29748caf88570affb9715e18b69) Migration algorithm with performance benefits

## gecko-dev
* [`d75b9ed4bd65`](https://github.com/mozilla/gecko-dev/commit/d75b9ed4bd655cd13096d6e4a8b305d37b74fa92)
* [`04238b5088cb`](https://github.com/mozilla/gecko-dev/commit/04238b5088cb5cad9ca227df58b977e7e0be6544)
* [`5803056b7004`](https://github.com/mozilla/gecko-dev/commit/5803056b7004e0669c65a9013ace27b25b61cbd2)
* [`863402e5363e`](https://github.com/mozilla/gecko-dev/commit/863402e5363e8e3a8eda98cd3c8f34345708f903) Do not do the entire compilation
* [`c8c196a72313`](https://github.com/mozilla/gecko-dev/commit/c8c196a723138b17af3039650cf6de4a0493250b)
* [`c04bd7aa0e75`](https://github.com/mozilla/gecko-dev/commit/c04bd7aa0e751891949409352ab804377026c9c1)
* [`5f834ddfb2e1`](https://github.com/mozilla/gecko-dev/commit/5f834ddfb2e13133e35afaae10a8e6e8a32f8b62)
* [`1266c84e30ef`](https://github.com/mozilla/gecko-dev/commit/1266c84e30efd23d0b6cd6b05042c87baab396c7) Lazy evaluation
* [`7bd233bc8795`](https://github.com/mozilla/gecko-dev/commit/7bd233bc87953567a688842ea82ea5c68b99af25)
* [`d66b6c2654d3`](https://github.com/mozilla/gecko-dev/commit/d66b6c2654d32a06082cb64e33c0bfca7f594fd7)
* [`9ea0de6975ea`](https://github.com/mozilla/gecko-dev/commit/9ea0de6975eaa174e727584620be0639d2ff4fa9)
* [`5c1b11f1db48`](https://github.com/mozilla/gecko-dev/commit/5c1b11f1db487390115dbd498204fc78ee68ef63) Do not shutdown the device when there're events pending
* [`5bcb89ae72f9`](https://github.com/mozilla/gecko-dev/commit/5bcb89ae72f92e9cbf27b09a9b86f2b93154e46c) Implementing faster rendering
* [`0edbe1a51235`](https://github.com/mozilla/gecko-dev/commit/0edbe1a51235b896aff6cc49f0d64abf7bab2440)
* [`661d20b507f4`](https://github.com/mozilla/gecko-dev/commit/661d20b507f40e7d23bae3df5d7e7f423cd7f41b)
* [`e222ca710936`](https://github.com/mozilla/gecko-dev/commit/e222ca7109362d2910abe8790e36ab1ffc73e8e8)
* [`755b99f4f670`](https://github.com/mozilla/gecko-dev/commit/755b99f4f670661813feef3c7a9336155a9edd5b)
* [`e53f74cb868d`](https://github.com/mozilla/gecko-dev/commit/e53f74cb868d9cd4908d44a3b14573e721b246b1)
* [`a5dd4da99496`](https://github.com/mozilla/gecko-dev/commit/a5dd4da994965ec5674fba719ed31087675fcb0e)
* [`32dbe991d685`](https://github.com/mozilla/gecko-dev/commit/32dbe991d685a905bbcc4b78fb7fa8fb1aa0ba8f) New `MaskSurface` facility
* [`ac0ae7443953`](https://github.com/mozilla/gecko-dev/commit/ac0ae744395312fdb9c0090927512287b894eac3)
* [`98cfb0e977a3`](https://github.com/mozilla/gecko-dev/commit/98cfb0e977a3511259ab76f41c2e4a9965fa0855) Avoid duplicated allocation
* [`761a458cda5c`](https://github.com/mozilla/gecko-dev/commit/761a458cda5cadc016cbcf71071b627e0ba6a029) Avoid repeated serialization/deserialization
* [`3a3d9ab7f782`](https://github.com/mozilla/gecko-dev/commit/3a3d9ab7f782990d2cde6876fe1d1c1ae6378e66)
* [`1755aa59667c`](https://github.com/mozilla/gecko-dev/commit/1755aa59667c8ae9095762f11b97c454eada997d)
* [`214e03003f17`](https://github.com/mozilla/gecko-dev/commit/214e03003f17f529690437119a779db953d3bd98)
* [`def1f6e7db52`](https://github.com/mozilla/gecko-dev/commit/def1f6e7db52f3a1f2929566bf1d48fdecd63370)
* [`fddb9e0b1d3b`](https://github.com/mozilla/gecko-dev/commit/fddb9e0b1d3b7cfc3653f3d5a3fd865923525d79) Disable a function at all
* [`26105ef609ee`](https://github.com/mozilla/gecko-dev/commit/26105ef609eeee32dd08851e55a08253b0618b5b) Move the rvalue reference
* [`046b03d3df31`](https://github.com/mozilla/gecko-dev/commit/046b03d3df31a4cd4182a0998bfd9cba539ff6a3) Move the rvalue reference
* [`7fccf7772218`](https://github.com/mozilla/gecko-dev/commit/7fccf77722188b706082866fcd60311a476e0c8a)
* [`b106718dcb4e`](https://github.com/mozilla/gecko-dev/commit/b106718dcb4e5ff972829ef6976687a0254a241a)
* [`0391a5005094`](https://github.com/mozilla/gecko-dev/commit/0391a5005094db4ee2e4c9fcd2529528ce428389)
* [`b6275dd517c1`](https://github.com/mozilla/gecko-dev/commit/b6275dd517c124256c1f1002a7260171cff54e90)
* [`ce7bb69227fd`](https://github.com/mozilla/gecko-dev/commit/ce7bb69227fd89f0b0fba6a840aee45686f7a3b3)
* [`e1f1db7272cc`](https://github.com/mozilla/gecko-dev/commit/e1f1db7272cc9f67e2dfee87ca529f86e40f9390) Do not request remote processes when restoring input context
* [`6adaf4dfcb7f`](https://github.com/mozilla/gecko-dev/commit/6adaf4dfcb7fae5b8d39536f54503f21681d91f4)
* [`655ade7a8b03`](https://github.com/mozilla/gecko-dev/commit/655ade7a8b033cd6d74fd24bf6b5fb7c91679b98)
* [`a7a4b9396837`](https://github.com/mozilla/gecko-dev/commit/a7a4b9396837c7d3daf5c65345edf8aac4a75c85) Prefetching
* [`0ffa8c1cd8fa`](https://github.com/mozilla/gecko-dev/commit/0ffa8c1cd8fa6ce781748d760f98ef028adf55fc)
* [`86ed8740c135`](https://github.com/mozilla/gecko-dev/commit/86ed8740c1359b31b652d161b93583bd229070fe)
* [`fc3e532636e1`](https://github.com/mozilla/gecko-dev/commit/fc3e532636e18de926d11974332fc4e32ae91975)
* [`2c5b4e5e64a2`](https://github.com/mozilla/gecko-dev/commit/2c5b4e5e64a2274b768fdd73cc1904f86870b9bf)
* [`4c1bd947157e`](https://github.com/mozilla/gecko-dev/commit/4c1bd947157efeddb9ef6783ec65f38002f8943e) Change hashtable to vector
* [`0ebdcdbf60d3`](https://github.com/mozilla/gecko-dev/commit/0ebdcdbf60d3dec020da3082f9521acab181eafe)
* [`7ca3c89ebb0b`](https://github.com/mozilla/gecko-dev/commit/7ca3c89ebb0be321827585792bfa93713e2118f1) Data structure
* [`783e661627ee`](https://github.com/mozilla/gecko-dev/commit/783e661627ee82083e5f2a71668ade7633dfb897)
* [`59eb07a88fb7`](https://github.com/mozilla/gecko-dev/commit/59eb07a88fb7b855798d6e81da481e79247627c7) Reduce the parameter passed into the function
* [`c7503566b286`](https://github.com/mozilla/gecko-dev/commit/c7503566b286f72c39ab298b9876eb31fc01a552)
* [`a2be039491cc`](https://github.com/mozilla/gecko-dev/commit/a2be039491cce06f96d10460bc0a9fb1cd0fc3ee) Merge all callbacks
* [`19d13525ebc7`](https://github.com/mozilla/gecko-dev/commit/19d13525ebc7839243866b0252b51d57088fa174) Put more common case at the lhs of a logic operator
* [`daab3fdaf71c`](https://github.com/mozilla/gecko-dev/commit/daab3fdaf71cb4f2c1a071abb9d0c7e888265ce3) ???
* [`8e3206382b84`](https://github.com/mozilla/gecko-dev/commit/8e3206382b847729e3a27a4fc9a7f0d5bfc6e5a0) Make the arguments passed through registers on x64
* [`ce294cea4266`](https://github.com/mozilla/gecko-dev/commit/ce294cea4266435bc7330f338e37318aa4eb2479)
* [`185af4c21b68`](https://github.com/mozilla/gecko-dev/commit/185af4c21b68999cab8f65a79a1a910bac894231) Compiler optimization

## openssl
* [`a091e212fc55`](https://github.com/openssl/openssl/commit/a091e212fc55244fe03a4c7db7d8978c5b5014cb)
* [`c1b2569d234c`](https://github.com/openssl/openssl/commit/c1b2569d234c1247d2a7a3338ca4568bc0a489a5)
* [`cfc32a1efb46`](https://github.com/openssl/openssl/commit/65d62488b8c808350f440d2276034f5223b391ad)
* [`06b9ff06cc7f`](https://github.com/openssl/openssl/commit/06b9ff06cc7fdd8f51abb92aaac39d3988a7090e) Change of API to reduce the context switch latency
* [`3064b5513443`](https://github.com/openssl/openssl/commit/3064b55134434a0b2850f07eff57120f35bb269a) Avoid reseeding
* [`d5487a454c48`](https://github.com/openssl/openssl/commit/d5487a454c485eb6f9aef7fb0cb1c0681a06fd25) pl
* [`10bc3409459a`](https://github.com/openssl/openssl/commit/10bc3409459a525654d6b986b3cd49d22dd95460) Static new algorithm path
* [`cfc32a1efb46`](https://github.com/openssl/openssl/commit/cfc32a1efb464205885e18e503bcb7051c307008) Static new algorithm path
* [`0f3ab9a34c0a`](https://github.com/openssl/openssl/commit/0f3ab9a34c0a30b5d471ad1c1ba88cc0e10dd74a) Static new algorithm path
* [`13603583b3cc`](https://github.com/openssl/openssl/commit/13603583b3cc7b7a33bf17dc6262e4bc103e917c) Static new algorithm path
* [`0dd0be9408a4`](https://github.com/openssl/openssl/commit/0dd0be9408a4f20941c7ec23b4634cdf6402f573) Static new algorithm path
* [`dc99b885ded3`](https://github.com/openssl/openssl/commit/dc99b885ded3cbc586d5ffec779f0e75a269bda3) Static new algorithm path
* [`a3ea6bf0ef70`](https://github.com/openssl/openssl/commit/a3ea6bf0ef703b38a656245931979c7e53c410b7)
* [`b98530d6e09f`](https://github.com/openssl/openssl/commit/b98530d6e09f4cb34c791b8840e936c1fc1467cf)
* [`79dfc3ddfd80`](https://github.com/openssl/openssl/commit/79dfc3ddfd80cd4294136b19fa48037e149cbd9e) Static new algorithm path
* [`5fc2c6896d50`](https://github.com/openssl/openssl/commit/5fc2c6896d5050735c7d99dc80275c72fc58c49c)
* [`e8b0dd57c0e9`](https://github.com/openssl/openssl/commit/e8b0dd57c0e9c53fd0708f0f458a7a2fd7a95c91)
* [`afa23c46d966`](https://github.com/openssl/openssl/commit/afa23c46d966fc3862804612be999d403a755cd7)
* [`c10e3f0cffb3`](https://github.com/openssl/openssl/commit/c10e3f0cffb3820da64a3f822cca96239efe4369) Avoid heavy `HMAC()` function
* [`1fab06a66533`](https://github.com/openssl/openssl/commit/1fab06a665333d4afdd2bc46a1ab787fd473ec49)
* [`b3e02d06ba80`](https://github.com/openssl/openssl/commit/b3e02d06ba802aaa137b5f5000b02c504b72cdcb)
* [`ae1ffe0f65c4`](https://github.com/openssl/openssl/commit/ae1ffe0f65c460ccdfe5153b96fe9943d7a171b8)
* [`26b05245f0d3`](https://github.com/openssl/openssl/commit/26b05245f0d3a6cea970f104f6aff388948fe318)
* [`0edb109f97c1`](https://github.com/openssl/openssl/commit/0edb109f97c1bbbd5961326f93b2ccf385b26674) Shortened buffer length for `ChaCha20_ctr32()`
* [`848113a30b43`](https://github.com/openssl/openssl/commit/848113a30b431c2fe21ae8de2a366b9b6146fb92) Have one call to a function instead of multiple calls
* [`a78324d95bd4`](https://github.com/openssl/openssl/commit/a78324d95bd4568ce2c3b34bfa1d6f14cddf92ef) Change the field of `r` to reduce the latency of `bn_pollute()`
* [`be2c4d9bd9e8`](https://github.com/openssl/openssl/commit/be2c4d9bd9e81030c547a34216ae2d8e5c888190) New algorithm to speed up certain computation
* [`1d2a18dc5a3b`](https://github.com/openssl/openssl/commit/1d2a18dc5a3b3363e17db5af8b6b0273856ac077) / [`a9c6edcde7f8`](https://github.com/openssl/openssl/commit/a9c6edcde7f8e874f177e55ea5f62b89646ea42c) multiblock performance imrpovement, using larger buffer allocated
* [`7823d792d0ca`](https://github.com/openssl/openssl/commit/7823d792d0cad3b44ad5389a8d3381becefe7f44) Use hash to check duplication
* [`cb8164b05e3b`](https://github.com/openssl/openssl/commit/cb8164b05e3bad5586c2a109bbdbab1ad65a1a6f) Const
* [`81f3d6323dcd`](https://github.com/openssl/openssl/commit/81f3d6323dcda6a18b06c718600d6a4739e83263) Split data for aligned and misaligned
* [`7caf122e717e`](https://github.com/openssl/openssl/commit/7caf122e717e79afcb986fe217e77a630b67bf4c)
* [`3fc7a9b96cbe`](https://github.com/openssl/openssl/commit/3fc7a9b96cbed0c3da6f53c08e34d8d0c982745f) Revert
* [`3712436071c0`](https://github.com/openssl/openssl/commit/3712436071c04ed831594cf47073788417d1506b)
* [`9d91530d2d7d`](https://github.com/openssl/openssl/commit/9d91530d2d7da1447b7be8631b269599023430e7)

## systemd
* [`a9becdd65bb4`](https://github.com/systemd/systemd/commit/a9becdd65bb4b64675bc0c109d14ab12b1ecd2b7)
* [`bf1d6be5b81c`](https://github.com/systemd/systemd/commit/bf1d6be5b81cc83c00cf4c40c7638bd13ea68cb1)
* [`8f2665a463ff`](https://github.com/systemd/systemd/commit/8f2665a463ffb2b822c1e3effdb292d2bddf72be)
* [`3c72b6ed4252`](https://github.com/systemd/systemd/commit/3c72b6ed4252e7ff5f7704bfe44557ec197b47fa)
* [`fd373593ba9b`](https://github.com/systemd/systemd/commit/fd373593ba9b7c8426529623a803086038db4474)
* [`aa40ff070391`](https://github.com/systemd/systemd/commit/aa40ff070391e1a5eebafcec834a59973fbaac00)
* [`539618a0ddc2`](https://github.com/systemd/systemd/commit/539618a0ddc2dc7f0fbe28de2ae0e07b34c81e60)
* [`bf516294c89d`](https://github.com/systemd/systemd/commit/bf516294c89d8db9769d90b4f90c177adafa038c)
* [`ed71f95662af`](https://github.com/systemd/systemd/commit/ed71f95662af903f0c5eba32c439e77c5cec4e7b)
* [`b872843c4db1`](https://github.com/systemd/systemd/commit/b872843c4db1bb8677c9e1ed8c8c65167181adba) Generate fewer logs
* [`ddb3706d2643`](https://github.com/systemd/systemd/commit/ddb3706d2643461a9b03044bd215e53c981d755b)
* [`ff02f101cb7d`](https://github.com/systemd/systemd/commit/ff02f101cb7db516bf1732bb74c42cb3b6259af1)
* [`b4a11878f2fd`](https://github.com/systemd/systemd/commit/b4a11878f2fdf5b07f895863747153de632ff4e6)
* [`a57f7e2c828b`](https://github.com/systemd/systemd/commit/a57f7e2c828b852eb32fd810dcea041bb2975501)
* [`4fcb507a909c`](https://github.com/systemd/systemd/commit/4fcb507a909c60d8568b17fc8a9dbb8c83029c01)
* [`244d2f07b49e`](https://github.com/systemd/systemd/commit/244d2f07b49e3470d679fdd0f6ebd24fac8d5dc7)
* [`daa557208de1`](https://github.com/systemd/systemd/commit/daa557208de1d77d4aa4f8f8fb162b10d796678f)
* [`4dc63c4bc785`](https://github.com/systemd/systemd/commit/4dc63c4bc7850bc570fcf33800a444e09dec81d5)
* [`7b943bb7e3e2`](https://github.com/systemd/systemd/commit/7b943bb7e3e2fad3481c7191edbfaeef0680b196) Faster API
* [`77fa610b22f3`](https://github.com/systemd/systemd/commit/77fa610b22f343671945501905e8079d68807ddb)
* [`fc9ae7178e14`](https://github.com/systemd/systemd/commit/fc9ae7178e1462377b272a14b1a763d480ab0980)
* [`6f8cbcdb27d7`](https://github.com/systemd/systemd/commit/6f8cbcdb27d772521ba71f92c25fd522efd56cf4)
* [`fe902fa496ab`](https://github.com/systemd/systemd/commit/fe902fa496abb4583c5befaf671a2402b650cd14)
* [`49bfc8774bf9`](https://github.com/systemd/systemd/commit/49bfc8774bf900fb2239a9b70b951aedccbfed5a)
* [`3bbaff3e0807`](https://github.com/systemd/systemd/commit/3bbaff3e08070f03487958818edbd161d439ce15)
* [`454f0f8680eb`](https://github.com/systemd/systemd/commit/454f0f8680ebbded135e8575b4d9615b427fdf76)
* [`8f1e0ad415da`](https://github.com/systemd/systemd/commit/8f1e0ad415dab8f3e5d301129fe2bb25c420e66f)
* [`fdb90ac6a6f3`](https://github.com/systemd/systemd/commit/fdb90ac6a6f3320a33104951e0c8505df901cc4f)
* [`ef9a3e3c2809`](https://github.com/systemd/systemd/commit/ef9a3e3c28095e52f8ffe96acf3c70b2babfacb5)
* [`4d247a6cd3f6`](https://github.com/systemd/systemd/commit/4d247a6cd3f69acbc5a09e8ac7e4fbb50eaa3228)
* [`422baca0f230`](https://github.com/systemd/systemd/commit/422baca0f230913158078fddf884e06c8c64a316)
* [`8372da448f3c`](https://github.com/systemd/systemd/commit/8372da448f3c738e0154d988538d497f7e2e1f83)
* [`a6149b93afeb`](https://github.com/systemd/systemd/commit/a6149b93afeb4e7b37e1313920ac2e0a91ff1c08)
* [`da50b85af729`](https://github.com/systemd/systemd/commit/da50b85af7299c19cb345bd0fa2aa2f19f176ff3) Use path as an indicator for the uid candidate. If we cannot use the uid of suggested paths, hash the path and get a random uid
* [`1086182d83d4`](https://github.com/systemd/systemd/commit/1086182d83d4c02a75f96f0184d5e8e5d3af6528) Test two sets linearly for equality instead of mutual superset
* [`93484b4790e2`](https://github.com/systemd/systemd/commit/93484b4790e261ac3dab12cba5b20d33c74a9202) New API that avoids `printf()` styled formatting
* [`426bb5ddb8ff`](https://github.com/systemd/systemd/commit/426bb5ddb8ff122d3e08b0480466718b68485e70)
* [`d4cdbea5550a`](https://github.com/systemd/systemd/commit/d4cdbea5550ae9defa1c731ffe091837d329fec7) Avoid waiting for the interface configuration at _boot time_
* [`708e25e137f8`](https://github.com/systemd/systemd/commit/708e25e137f8938791b72194a3d56c3476793175)
* [`5e5420d1a102`](https://github.com/systemd/systemd/commit/5e5420d1a10259ac416171e2874a118418f669a6)
* [`c79e98eadd30`](https://github.com/systemd/systemd/commit/c79e98eadd3056a36a662699fa650db5b1bca0c3) Use memfds instead of shm
* [`7943f4227502`](https://github.com/systemd/systemd/commit/7943f42275025e1b6642b580b19b24dfab8dee61) / [`487d37209b30`](https://github.com/systemd/systemd/commit/487d37209b30a536636c95479cfeba931fea25c5)
* [`e32530cbef74`](https://github.com/systemd/systemd/commit/e32530cbef746a3d346334a821a7c804f49af4e6)
* [`4b64536ee7fd`](https://github.com/systemd/systemd/commit/4b64536ee7fd0438c93d1784824098f826cd642c) Alloc
* [`a257f9d4a53e`](https://github.com/systemd/systemd/commit/a257f9d4a53e98da6306b674d2cbb63b42d67d20)
* [`2d058a87ffb2`](https://github.com/systemd/systemd/commit/2d058a87ffb2d31a50422a8aebd119bbb4427244)
* [`cc25a67e2af2`](https://github.com/systemd/systemd/commit/cc25a67e2af21833f9fef5aba6e16beafa03b0c7)
* [`bcf3ce7b3952`](https://github.com/systemd/systemd/commit/bcf3ce7b39529eb5421aa45d6336c9b12f8594c0)
* [`66a67effcc5b`](https://github.com/systemd/systemd/commit/66a67effcc5beaf8a61e1c1147c3114b02a96439)
* [`acf7f253dedb`](https://github.com/systemd/systemd/commit/acf7f253dedbca7ba5edada37d11221a47101ee3)
* [`47b33c7d5284`](https://github.com/systemd/systemd/commit/47b33c7d5284492e5679ccfabafe8c9738de8cb3)
* [`496ae8c84b2d`](https://github.com/systemd/systemd/commit/496ae8c84b2d3622bc767a727e3582e2b6bcffcd)
* [`00a8cf7763ec`](https://github.com/systemd/systemd/commit/00a8cf7763ec5e132efd4c974fbc6530c82240d0)
* [`6fe869c25179`](https://github.com/systemd/systemd/commit/6fe869c251790a0e3cef5b243169dda363723f49) Drop stability guarantee
* [`081f36d82de8`](https://github.com/systemd/systemd/commit/081f36d82de848ab93acbc19cac0d79186e9321c) Elimiate spaces when encoding/decoding base64
* [`fb5c8216646e`](https://github.com/systemd/systemd/commit/fb5c8216646ecfb8b13b4867094c7032242de765) 2 msg commit for faster client conf
* [`8621b1109b0b`](https://github.com/systemd/systemd/commit/8621b1109b0bbe097f2b3bfce2211fe8474c567b) Set keep-free value to avoid some SSD's performance degradation when free space is less than this value
* [`ff0e7e05c990`](https://github.com/systemd/systemd/commit/ff0e7e05c9904b4120868e96216b123c1b798aa4)
* [`8e060ec225b7`](https://github.com/systemd/systemd/commit/8e060ec225b74bbf22e5bdbacd604efcc73294c0)
* [`1930eed2a785`](https://github.com/systemd/systemd/commit/1930eed2a7855d2df06ccf51f9e394428bf547e2)
* [`8580d1f73db3`](https://github.com/systemd/systemd/commit/8580d1f73db36e9383e674e388b4fb55828c0c66) Limit the number of files
* [`e12020472976`](https://github.com/systemd/systemd/commit/e120204729764f6243b60899eb907103e678bee2) Filter the list then processing
* [`94243ef29942`](https://github.com/systemd/systemd/commit/94243ef299425d6c7089a7a05c48c9bb8f6cf3da)
* [`df0ff1277588`](https://github.com/systemd/systemd/commit/df0ff127758809a45105893772de76082d12a26d) / [`5c30a6d2b805`](https://github.com/systemd/systemd/commit/5c30a6d2b805ae9b5dd0ad003b9ee86b8965bc47)
* [`ae5b39588734`](https://github.com/systemd/systemd/commit/ae5b39588734aacef5b6b411cf2053fdcef4764a)
* [`17c8de633faa`](https://github.com/systemd/systemd/commit/17c8de633faad3ac97012e066c6c6b2f71b83a67)
* [`815b09d39b74`](https://github.com/systemd/systemd/commit/815b09d39b74a9ece31b7d7ef45f8f00784b8d8b)
* [`52b147841406`](https://github.com/systemd/systemd/commit/52b1478414067eb9381b413408f920da7f162c6f)
* [`6414b7c98137`](https://github.com/systemd/systemd/commit/6414b7c981378a6eef480f6806d7cbfc98ca22a1)
* [`f268980d2cee`](https://github.com/systemd/systemd/commit/f268980d2cee694fa4118a71402a47c316af0425)
* [`04c4d9199e04`](https://github.com/systemd/systemd/commit/04c4d9199e04eb0ec2527ef34f182072d11a9338) Cache TLS/TFO written data
* [`5659958529d1`](https://github.com/systemd/systemd/commit/5659958529d16f082a24d0c5b68699570b3eace3)
* [`8a5cd31e5fa0`](https://github.com/systemd/systemd/commit/8a5cd31e5fa0b1313a196b902e4b3c4603e7dfdf)
* [`6febe75da765`](https://github.com/systemd/systemd/commit/6febe75da76517a69f073fb50abffc5c2c7d58cd) Macro makes string length as const
* [`da0c630e141e`](https://github.com/systemd/systemd/commit/da0c630e141e3c3fab633a1c7a0686295e2c9411) Data structure
* [`91ccab1e40a1`](https://github.com/systemd/systemd/commit/91ccab1e40a10963764f449ba8309d47e90d6a8a) Add fast connection
* [`c3950a9bbe0d`](https://github.com/systemd/systemd/commit/c3950a9bbe0d6f8ddd735a6f902281a3b2b4ca15) Algo
* [`6dbef3053dcb`](https://github.com/systemd/systemd/commit/6dbef3053dcb11a58ead99bee0790ad1f4c40522)
* [`5d67a7ae749d`](https://github.com/systemd/systemd/commit/5d67a7ae749deb744125de9301cc71689a22648a) Algo
* [`4b5bc5396c09`](https://github.com/systemd/systemd/commit/4b5bc5396c090ee41c45cab9052372d296c4a2f4) Use new compression algorithm
* [`b29ddfcb3891`](https://github.com/systemd/systemd/commit/b29ddfcb389127cf00ab41447a721e479fe15713) Author does not know why

## libgcrypt
* [`f9371c026aad`](https://github.com/gpg/libgcrypt/commit/f9371c026aad09ff48746d22c8333746c886e773)
* [`6e669e09603e`](https://github.com/gpg/libgcrypt/commit/6e669e09603e5a98b59dcf35f77f346db6c81eac) / [`b6e6ace32444`](https://github.com/gpg/libgcrypt/commit/b6e6ace324440f564df664e27f8276ef01f76795)
* [`a15c1def7e0f`](https://github.com/gpg/libgcrypt/commit/a15c1def7e0f170f6663635db84fecab1cbfcca7) Use optimized APIs
* [`ca21a24808ef`](https://github.com/gpg/libgcrypt/commit/ca21a24808efa5d562ac91f683504ae0d6dfa69f)
* [`3841b23c0ccb`](https://github.com/gpg/libgcrypt/commit/3841b23c0ccb24d555b7570083bba958e3126d26)
* [`656395ba4cf3`](https://github.com/gpg/libgcrypt/commit/656395ba4cf34f42dda3a120bda3ed1220755a3d)
* [`20886fdcb841`](https://github.com/gpg/libgcrypt/commit/20886fdcb841b0bf89bb1d44303d42f1804e38cb)
* [`bf6d5b10cb41`](https://github.com/gpg/libgcrypt/commit/bf6d5b10cb4173826f47ac080506b68bb001acb2)
* [`49f52c67fb42`](https://github.com/gpg/libgcrypt/commit/49f52c67fb42c0656c8f9af655087f444562ca82)
* [`e950052bc6f5`](https://github.com/gpg/libgcrypt/commit/e950052bc6f5ff11a7c23091ff3f6b5cc431e875) Remove unaccelerated paths
* [`305cc878d395`](https://github.com/gpg/libgcrypt/commit/305cc878d395475c46b4ef52f4764bd0c85bf8ac) Add accelerated aptah
* [`04cda6b7cc16`](https://github.com/gpg/libgcrypt/commit/04cda6b7cc16f3f52c12d9d3e46c56701003496e)
* [`293e93672fda`](https://github.com/gpg/libgcrypt/commit/293e93672fdabc829e35cc624c397276342bafe4)
* [`4645f3728bb0`](https://github.com/gpg/libgcrypt/commit/4645f3728bb0900591b0aef85831fdee52c59e3c) No need to copy all param
* [`86e5e06a97ae`](https://github.com/gpg/libgcrypt/commit/86e5e06a97ae13b8bbf6923ecc76e02b9c429b46) Use proper sized objects so that GCC can optimize that
* [`fe91a642c7c2`](https://github.com/gpg/libgcrypt/commit/fe91a642c7c257aca095b96406fbcace88fa3df4) Signing parses shorter strings with the option
* [`634a85412a40`](https://github.com/gpg/libgcrypt/commit/634a85412a4073aa1890589ce5e97eac7b0f3ca3)
* [`6571a6433183`](https://github.com/gpg/libgcrypt/commit/6571a64331839d7d952292163afbf34c8bef62e0)
* [`c59a8ce51ceb`](https://github.com/gpg/libgcrypt/commit/c59a8ce51ceb9a80169c44ef86a67e95cf8528c3) Use avx2 asm for faster twofish cipher computation
* [`5c418e597f0f`](https://github.com/gpg/libgcrypt/commit/5c418e597f0f20a546d953161695e6caf1f57689) asm optimization
* [`2d2e5286d53e`](https://github.com/gpg/libgcrypt/commit/2d2e5286d53e1f62fe040dff4c6e01961f00afe2) asm optimization
* [`63ac3ba07dba`](https://github.com/gpg/libgcrypt/commit/63ac3ba07dba82fde040d31b90b4eff627bd92b9) asm
* [`27747921cb1d`](https://github.com/gpg/libgcrypt/commit/27747921cb1dfced83c5666cd1c474764724c52b) asm
* [`5418d9ca4c0e`](https://github.com/gpg/libgcrypt/commit/5418d9ca4c0e087fd6872ad350a996fe74880d86) asm
* [`de73a2e7237b`](https://github.com/gpg/libgcrypt/commit/de73a2e7237ba7c34ce48bb5fb671aa3993de832) asm
* [`0b3ec359e227`](https://github.com/gpg/libgcrypt/commit/0b3ec359e2279c3b46b171372b1b7733bba20cd7) Use intel asm for faster sha256
* [`d02958bd300d`](https://github.com/gpg/libgcrypt/commit/d02958bd300d2c80bc92b1e072103e95e256b297) Use intel asm for faster sha1
* [`da58a62ac1b7`](https://github.com/gpg/libgcrypt/commit/da58a62ac1b7a8d97b0895dcb41d15af531e45e5) Use avx asm for faster blake2s
* [`af7fc732f9a7`](https://github.com/gpg/libgcrypt/commit/af7fc732f9a7af7a70276f1e8364d2132db314f1) Use avx2 asm for faster blake2s
* [`93503c127a52`](https://github.com/gpg/libgcrypt/commit/93503c127a52c1f6a193750e2bf181a744ba3e6b) Use arm8/ce asm for faster aes
* [`a00c5b2988ce`](https://github.com/gpg/libgcrypt/commit/a00c5b2988cea256c7823a76ce601febf02c790f) Use aes-ni (an instruction set) asm for faster aes
* [`c9e9cb2eb6a1`](https://github.com/gpg/libgcrypt/commit/c9e9cb2eb6a1c659d3825ca627228b732f2f2152) Use aes-ni (an instruction set) asm for faster aes
* [`c85501af8222`](https://github.com/gpg/libgcrypt/commit/c85501af8222913f0a1e20e77fceb88e93417925)
* [`b3ec0f752c92`](https://github.com/gpg/libgcrypt/commit/b3ec0f752c925cde36f560f0f9309ab6450bbfd9) aarch64 asm to accelerate chacha20
* [`0b332c1aef03`](https://github.com/gpg/libgcrypt/commit/0b332c1aef03a735c1fb0df184f74d523deb2f98)
* [`2d4bbc0ad62c`](https://github.com/gpg/libgcrypt/commit/2d4bbc0ad62c54bbdef77799f9db82d344b7219e)
* [`e4eb03f56683`](https://github.com/gpg/libgcrypt/commit/e4eb03f56683317c908cb55be727832810dc8c72)
* [`595251ad37bf`](https://github.com/gpg/libgcrypt/commit/595251ad37bf1968261d7e781752513f67525803)
* [`962b15470663`](https://github.com/gpg/libgcrypt/commit/962b15470663db11e5c35b86768f1b5d8e600017)
* [`34c64eb03178`](https://github.com/gpg/libgcrypt/commit/34c64eb03178fbfd34190148fec5a189df2b8f83)
* [`98f021961ee6`](https://github.com/gpg/libgcrypt/commit/98f021961ee65669037bc8bb552a69fd78f610fc)
* [`3d6334f8d94c`](https://github.com/gpg/libgcrypt/commit/3d6334f8d94c2a4df10eed203ae928298a4332ef)
* [`5d601dd57fcb`](https://github.com/gpg/libgcrypt/commit/5d601dd57fcb41aa2015ab655fd6fc51537da667)
* [`8353884bc65c`](https://github.com/gpg/libgcrypt/commit/8353884bc65c820d5bcacaf1ac23cdee72091a09)
* [`a1cc7bb15473`](https://github.com/gpg/libgcrypt/commit/a1cc7bb15473a2419b24ecac765ae0ce5989a13b)
* [`2857cb89c6dc`](https://github.com/gpg/libgcrypt/commit/2857cb89c6dc1c02266600bc1fd2967a3cd5cf88)
* [`909644ef5883`](https://github.com/gpg/libgcrypt/commit/909644ef5883927262366c356eed530e55aba478)
* [`2fd06e207dce`](https://github.com/gpg/libgcrypt/commit/2fd06e207dcea1d8a7f0e7e92f3359615a99421b)
* [`e11895da1f4a`](https://github.com/gpg/libgcrypt/commit/e11895da1f4af9782d89e92ba2e6b1a63235b54b)
* [`4f46374502eb`](https://github.com/gpg/libgcrypt/commit/4f46374502eb988d701b904f83819e2cf7b1755c)
* [`30bd759f398f`](https://github.com/gpg/libgcrypt/commit/30bd759f398f45b04d0a783b875f59ce9bd1e51d)
* [`323b1eb80ff3`](https://github.com/gpg/libgcrypt/commit/323b1eb80ff3396d83fedbe5bba9a4e6c412d192)
* [`297532602ed2`](https://github.com/gpg/libgcrypt/commit/297532602ed2d881d8fdc393d1961068a143a891)
* [`a39ee7555691`](https://github.com/gpg/libgcrypt/commit/a39ee7555691d18cae97560f130aaf952bfbd278)
* [`def7d4cad386`](https://github.com/gpg/libgcrypt/commit/def7d4cad386271c6d4e2f10aabe0cb4abd871e4)
* [`df629ba53a66`](https://github.com/gpg/libgcrypt/commit/df629ba53a662427ebd3ddca90c3fe9ddd6511d3)
* [`a5c2bbfe0db5`](https://github.com/gpg/libgcrypt/commit/a5c2bbfe0db515d739ab683297903c77b1eec124)
* [`e4e458465b12`](https://github.com/gpg/libgcrypt/commit/e4e458465b124e25b6aec7a60174bf1ca32dc5fd)
* [`6fd0dd2a5f13`](https://github.com/gpg/libgcrypt/commit/6fd0dd2a5f1362f91e2861cd9d300341a43842a5)
* [`2e4253dc8eb5`](https://github.com/gpg/libgcrypt/commit/2e4253dc8eb512cd0e807360926dc6ba912c95b4)
* [`be2238f68abc`](https://github.com/gpg/libgcrypt/commit/be2238f68abcc6f2b4e8c38ad9141376ce622a22)
* [`69a6d0f9562f`](https://github.com/gpg/libgcrypt/commit/69a6d0f9562fcd26112a589318c13de66ce1700e)
* [`e1a3931263e6`](https://github.com/gpg/libgcrypt/commit/e1a3931263e67aacec3c0bfcaa86c7d1441d5c6a)
* [`3ef21e7e1b80`](https://github.com/gpg/libgcrypt/commit/3ef21e7e1b8003db9792155044db95f9d9ced184)
* [`f702d62d888b`](https://github.com/gpg/libgcrypt/commit/f702d62d888b30e24c19f203566a1473098b2b31)
* [`b60f06f70227`](https://github.com/gpg/libgcrypt/commit/b60f06f70227c1e69e1010da8b47ea51ade48145) Parallelize
* [`319ee14f2aab`](https://github.com/gpg/libgcrypt/commit/319ee14f2aab8db56a830fd7ac8926f91b4f738a) Parallelize the computation
* [`305cc878d395`](https://github.com/gpg/libgcrypt/commit/305cc878d395475c46b4ef52f4764bd0c85bf8ac) Parallelize the computation
* [`99d15543b8d9`](https://github.com/gpg/libgcrypt/commit/99d15543b8d94a8f1ef66c6ccb862b0ce82c514d) Parallelize the computation
* [`2cb6e1f323d2`](https://github.com/gpg/libgcrypt/commit/2cb6e1f323d24359b1c5b113be5c2f79a2a4cded)
* [`3ff9d2571c18`](https://github.com/gpg/libgcrypt/commit/3ff9d2571c18cd7a34359f9c60a10d3b0f932b23)
* [`5a3d43485efd`](https://github.com/gpg/libgcrypt/commit/5a3d43485efdc09912be0967ee0a3ce345b3b15a)
* [`2fd83faa876d`](https://github.com/gpg/libgcrypt/commit/2fd83faa876d0be91ab7884b1a9eaa7793559eb9)
* [`98674fdaa30a`](https://github.com/gpg/libgcrypt/commit/98674fdaa30ab22a3ac86ca05d688b5b6112895d)
* [`1d85452412b6`](https://github.com/gpg/libgcrypt/commit/1d85452412b65e7976bc94969fc513ff6b880ed8)
* [`c030e33533fb`](https://github.com/gpg/libgcrypt/commit/c030e33533fb819afe195eff5f89ec39863b1fbc)
* [`6deb0ccdf718`](https://github.com/gpg/libgcrypt/commit/6deb0ccdf718a0670f80e6762a3842caf76437d6) Parallelize
* [`cafadc1e4fb9`](https://github.com/gpg/libgcrypt/commit/cafadc1e4fb97581262b0081ba251e05613d4394)
* [`31e4b1a96a07`](https://github.com/gpg/libgcrypt/commit/31e4b1a96a07e9a3698fcb7be0643a136ebb8e5c)
* [`8d1faf567145`](https://github.com/gpg/libgcrypt/commit/8d1faf56714598301580ce370e0bfa6d65e73644)
* [`f365961422f1`](https://github.com/gpg/libgcrypt/commit/f365961422f1c8b3d89b8bcd9c99828f38c1f158)
* [`71dda4507053`](https://github.com/gpg/libgcrypt/commit/71dda4507053379433dc8b0fc6462c15de7299df)
* [`d94ec5f5f8a5`](https://github.com/gpg/libgcrypt/commit/d94ec5f5f8a5d40a7d344025aa466f276f9718df)
* [`e7ab4e1a7396`](https://github.com/gpg/libgcrypt/commit/e7ab4e1a7396f4609b9033207015b239ab4a5140)
* [`d325ab5d86e6`](https://github.com/gpg/libgcrypt/commit/d325ab5d86e6107a46007a4d0131122bbd719f8c)
* [`7317fcfadf00`](https://github.com/gpg/libgcrypt/commit/7317fcfadf00789df140e51c0d16b60f6b144b59)
* [`0bdf26eea8cd`](https://github.com/gpg/libgcrypt/commit/0bdf26eea8cdbffefe7e37578f8f896c4f5f5275)
* [`2374753938df`](https://github.com/gpg/libgcrypt/commit/2374753938df64f6fd8015b44613806a326eff1a) Data structure
* [`8725c99ffa41`](https://github.com/gpg/libgcrypt/commit/8725c99ffa41778f382ca97233183bcd687bb0ce) Algo
* [`4de62d806442`](https://github.com/gpg/libgcrypt/commit/4de62d80644228fc5db2a9f9c94a7eb633d8de2e) copy block to stack for compiler optimization
* [`4b7451d3e8e7`](https://github.com/gpg/libgcrypt/commit/4b7451d3e8e7b87d8e407fbbd924ad5b13bd0f00) likely/unlikely
* [`efa9042f82ff`](https://github.com/gpg/libgcrypt/commit/efa9042f82ffed3d076b8e26ac62d29e00bb756a) Block alignment
* [`e7b941c3de9c`](https://github.com/gpg/libgcrypt/commit/e7b941c3de9c9b6319298c02f844cc0cadbf8562) Compiler optimization
* [`bf9e0b79e620`](https://github.com/gpg/libgcrypt/commit/bf9e0b79e620ca2324224893b07522462b125412) Compiler optimization
* [`925d4fb3e8f2`](https://github.com/gpg/libgcrypt/commit/925d4fb3e8f2df3c5566ec6b5df7620a3d3504e5) Compiler optimization
* [`f3e511610363`](https://github.com/gpg/libgcrypt/commit/f3e51161036382429c3491c7c881f36c0a653c7b)
* [`89fa74d6b3e5`](https://github.com/gpg/libgcrypt/commit/89fa74d6b3e58cd4fcd6e0939a35e46cbaca2ea0) Faster implementation with macro
* [`74184c28fbe7`](https://github.com/gpg/libgcrypt/commit/74184c28fbe7ff58cf57f0094ef957d94045da7d)
* [`f7505b550dd5`](https://github.com/gpg/libgcrypt/commit/f7505b550dd591e33d3a3fab9277c43c460f1bad)
* [`06e122baa332`](https://github.com/gpg/libgcrypt/commit/06e122baa3321483a47bbf82fd2a4540becfa0c9) Algo
* [`bd4bd23a2511`](https://github.com/gpg/libgcrypt/commit/bd4bd23a2511a4bce63c3217cca0d4ecf0c79532) Algo
* [`0e9e7d72f3c9`](https://github.com/gpg/libgcrypt/commit/0e9e7d72f3c9eb7ac832746c3034855faaf8d02c) Algo
* [`51501b638546`](https://github.com/gpg/libgcrypt/commit/51501b638546665163bbb85a14308fdb99211a28) Algo
* [`a48d07ccadee`](https://github.com/gpg/libgcrypt/commit/a48d07ccadee4cb8b666a9a4ba2f00129bad5b2f) Algo
* [`8e1c0f9b894c`](https://github.com/gpg/libgcrypt/commit/8e1c0f9b894c39b6554c544208dc000682f520c7) Algo
* [`ba892a0a874c`](https://github.com/gpg/libgcrypt/commit/ba892a0a874c8b2a83dbf0940608cd7e2911ce01)
* [`37d0a1ebdc2d`](https://github.com/gpg/libgcrypt/commit/37d0a1ebdc2dc74df4fb6bf0621045018122a68f) Algo
* [`ec2f8de409a9`](https://github.com/gpg/libgcrypt/commit/ec2f8de409a93c80efa658134df22074a9bca5a4) Algo

## linux
* [`cb0a8d23024e`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=cb0a8d23024e7bd234dea4d0fc5c4902a8dda766) Fallback to `vmalloc()` when continueous memory cannot be quickly returned
* [`48ad1abef402`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=48ad1abef40226ce809e5b7d3a5898754c4b9a9a) Use zram based `memset_l()`
* [`a339b351304d`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=a339b351304d5e6b02c7cf8eed895d181e64bce0) Avoid weighted affinity wake when the scheduler allows using idle CPU
* [`fbd02630c6e3`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=fbd02630c6e3c60feecc4688f5f98b015d264516) Avoid heavy weight `__kmem_cache_free_bulk()`
* [`6622f5cdbaf3`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=6622f5cdbaf3786314d76969d2aab132b36ba2e8) Avoid linear search on the corresponding device
* [`0a672f74131d`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=0a672f74131dd682087dfd5f45bf61f95804772e)
* [`03486830c577`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=03486830c577d3fe49c1f2c316414552a549ff00) Avoid size alignment
* [`9e641bdcfa4e`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=9e641bdcfa4ef4d6e2fbaa59c1be0ad5d1551fd5)
* [`7fdd29d02e0a`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=7fdd29d02e0ab595a857fe9c7b71e752ff665372) Do not lock on the entire send path tree
* [`9344de1b1c64`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=9344de1b1c64b2217f3b15508266deff2a8d6c84) Avoid getting cpuvar from thread hardware mapping
* [`4e6d47206c32`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=4e6d47206c32d1bbb4931f1d851dae3870e0df81) Split allocation in different places
* [`f379fdf10b12`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=f379fdf10b12e19080ee4e2ce27ed54d9e8806d8) Use idr instead of list
* [`4f72adc50682`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=4f72adc5068294268387a81a6bf91d9bb07ecc5c) Just return the cycles, leaving human readable formatting to call site
* [`66feed61cdf6`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=66feed61cdf6ee65fd551d3460b1efba6bee55b8) Fast signaling
* [`bbddca8e8fac`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=bbddca8e8fac07ece3938e03526b5d00fa791a4c) Avoid locking
* [`a60a132e89cd`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=a60a132e89cdb52621d154c78b69f7eb1307524d) Turn off the carrier earlier than data sync
* [`1ed7b84b28c1`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=1ed7b84b28c1a808d70e0b189febca5bc7e58fd7) Remove duplicated copy operation
* [`06e386a1db54`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=06e386a1db54ab6a671e103e929b590f7a88f0e3) Avoid irq save operation
* [`586a32ac1d33`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=586a32ac1d33ce7a7548a27e4087e98842c3a06f)
* [`8be2748a40cb`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=8be2748a40cb84c2ac5085e7e6bccb5a5fcc6235)
* [`a3795208b9a9`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=a3795208b9a9801a66b305395e9ebaae850eee03) Validate without linearization
* [`2889caa92321`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=2889caa9232109afc8881f29a2205abeb5709d0c)
* [`362108b5addd`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=362108b5adddac6f496acc40696e499defd56d62) Avoid lock
* [`09772d92cd5a`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=09772d92cd5ad998b0d5f6f46cd1658f8cb698cf) Avoid instrumenting extra function call in some condition
* [`ca47e8c72ae1`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=ca47e8c72ae141587cabf3dab693f6754d8c416b)
* [`a7f58b9ecfd3`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=a7f58b9ecfd3c0f63703ec10f4a592cc38dbd1b8)
* [`9226b5b440f2`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=9226b5b440f2b4fbb3b797f3cb74a9a627220660)
* [`2a4c32edd39b`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=2a4c32edd39b7de166e723b1991abcde4db3a701) Fastfail support to improve I/O latency
* [`cf0523350c6f`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=cf0523350c6f12bdffb06c7000326edb296ec450)
* [`c435ee34551e`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=c435ee34551e1f5a02a253ca8e235287efd2727c) Avoid callback lookup on the server side
* [`bef3efbeb897`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=bef3efbeb897b56867e271cdbc5f8adaacaeb9cd) Limit the rate of read which will broadcast SMI affecting the entire system
* [`f872f5400cc0`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=f872f5400cc01373d8e29d9c7a5296ccfaf4ccf3) Avoid returning a list of `struct pages` so that arch code won't walk the entire list
* [`20b3d54ecba5`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=20b3d54ecba51c5fe476eea94ffdc463559c5c85) Add the proxy layer to issue fewer commands to the hardware on non DirectLPI cases
* [`c9fefa08190f`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=c9fefa08190fc879fb2e681035d7774e0a8c5170)
* [`291a00d1a70f`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=291a00d1a70f96b393da9ac90c58a82bc7949fc8) Tweaking an argument for fast recovery
* [`eec7e1c16d2b`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=eec7e1c16d2b65e38137686dd9b7e102c2150905) Return the correct error code to minimize retries
* [`b83bd893f3a0`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=b83bd893f3a04d35f5bfcf399c1034660e5b2403) Reduce the delay
* [`9f7714d46383`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=9f7714d4638382d5f84fefd322983925935da742) Introducing new `struct` to send memory pages to video controller
* [`351a4dedb34c`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=351a4dedb34cbeb9f747f0e2309e891b6fb906cb)
* [`a5243dabb95c`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=a5243dabb95c51a4b2dce3f7e4f3ced57d2c5742)
* [`6c308fecb4d1`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=6c308fecb4d1f928d52f9586d976f79b37149388)
* [`d409014e4fee`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=d409014e4feeab486fb36b350abfc4c94de8be37)
* [`12d82e4b0aa6`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=12d82e4b0aa6c71f38b668a372f9a13f243207da)
* [`47df3ddedd22`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=47df3ddedd22c3f8e68aff831edb7921937674a2)
* [`bcd5642607ab`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=bcd5642607ab9195e22a1617d92fb82698d44448)
* [`856661d514e8`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=856661d514e8afc452bcda6f4c05a957694fea78)
* [`2ab91adae722`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=2ab91adae722c63d1a4c9a6e414636e34804d792)
* [`1ce42845f987`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=1ce42845f987e92eabfc6e026d44d826c25c74a5)
* [`7ef5ca4fe41e`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=7ef5ca4fe41e6a51d9bdcbbf7c66f203675e8500)
* [`3568bdf0419f`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=3568bdf0419fcaeebc5bba7cb2e034436b3e4125)
* [`759f29b62fb9`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=759f29b62fb9af5274e7f761f9f4cdfa7bb5a1f2)
* [`2d550dbad83c`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=2d550dbad83c88fc7cb594a1803e77457fe625f9)
* [`225785aec726`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=225785aec726f3edd5077be8f084b0b70ca197a8)
* [`0c7c1bed7e13`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=0c7c1bed7e13dbb545375c231e6ba1dca5e8d725) Use RCU
* [`8cb7c15b3254`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=8cb7c15b32545f9e7c4f72ffa357353109967a7a) Put the loop into the CS to avoid repeated lock acquisition
* [`cc91210cd2f6`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=cc91210cd2f6832db1a174269627d97b7d4f2c80) Use RCU
* [`5a27aa822029`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=5a27aa8220290b64cd5da066a1e29375aa867e69) Introducing kref for lock optimization
* [`97334162e4d7`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=97334162e4d79f866edd7308aac0ab3ab7a103f7) Prefetch
* [`8308756f45a1`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=8308756f45a12e2ff4f7749c2694fc83cdef0be9) Some microoptimization including avoiding locking on an internal structure
* [`e502a2af4c35`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=e502a2af4c358d14ecf8fce51bf4988ebb4d10b4) Use radix tree instead of hash table
* [`53e617e3dd5f`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=53e617e3dd5f97647f8ed6156267ca9c50c281bd) Data structure related
* [`b4f501916ce2`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=b4f501916ce2ae80c28017814d71d1bf83679271) Use raw CPU ptr
* [`f7d9afea6cfb`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=f7d9afea6cfb344021359ddd6101deb8f6e9cc76) Generate more efficient mips asm code
* [`fe31fca35d6a`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=fe31fca35d6af9176eec0024fac2ceeaacbc8639) Div optimization
* [`3d66c6ba3f97`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=3d66c6ba3f978fa88d62b83ad35e9adc31c8ea9e) Use per-cpu pointer
* [`abbdb5a74cea`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=abbdb5a74cead60e20b79c960c1772955f0b6b81) Removing `unlikely`
* [`9def970eada8`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=9def970eada83e6800cb4612ffdcd8eb7908c7b4)
* [`ad6048325c78`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=ad6048325c7807818c6c49e485660143d97a622e) 32-bit division faster than 64-bit
* [`bdfc7cbdeef8`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=bdfc7cbdeef8cadba0e5793079ac0130b8e2220c)
* [`2671e9fc62c3`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=2671e9fc62c392abea6a5e80297dfb03fdbae2e7) Use atomic variable by default unless `REFCOUNT_FULL` is enabled
* [`57a2af6bbc7a`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=57a2af6bbc7a4f1b145cc216c34476402836f0b8)
* [`20dcfe1b7df4`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=20dcfe1b7df4072a3c13bdb7506f7138125d0099) Align cacheline
* [`c17b0aadb7d8`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=c17b0aadb7d8f87de56a4a374a8131519c0f7422) Weaker form of barrier
* [`bbe08c0a43e2`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=bbe08c0a43e2c5ee3a00de68c0e867a08a9aa990) Microoptimization
* [`d70b3ef54cea`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=d70b3ef54ceaf1c7c92209f5a662a670d04cbed9) Support write-through cached memory mapping
* [`cffb78b0e0b3`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=cffb78b0e0b3a30b059b27a1d97500cf6464efa9)
* [`545ac13892ab`](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=545ac13892ab391049a92108cf59a0d05de7e28c)
