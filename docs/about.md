# About

This page is hosted at <https://gitlab.com/yqchen/perf-bugs>.

## Authors

* Yiqun Chen <ychen@cs.tu-darmstadt.de>
* Stefan Winter <sw@cs.tu-darmstadt.de>
* Neeraj Suri <suri@cs.tu-darmstadt.de>

## License

This data set is distributed under GPL license: <https://gitlab.com/yqchen/perf-bugs/blob/master/LICENSE>.
